#ifndef RENDER_H
#define RENDER_H


#include "include/model/matrixcolor.h"
#include <QMainWindow>

namespace Ui {
class Render;
}

class Render : public QMainWindow
{
    Q_OBJECT

public:
    explicit Render(QWidget *parent = 0);
    void raytrace(MatrixColor* m);
    ~Render();

private:
    Ui::Render *ui;
};

#endif // RENDER_H
