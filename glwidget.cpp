#include "glwidget.h"

#include "include/model/scene.h"
#include "include/primitives/cube.h"
#include "include/components/predefined.h"
#include "include/components/rendering.h"
#include "include/components/transform_component.h"
#include "include/components/matrix_component.h"

Scene *s = new Scene();

GLWidget::GLWidget(QWidget* parent):
    QGLWidget(parent)
{
    connect( &timer, SIGNAL(timeout()), this, SLOT(updateGL()) );

    timer.start(16);
}

void GLWidget::paintGL(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
/*
    render::get()->camworld();
    render::get()->getLight()->transform(transform::rotate(2, Vertex(0,1,0)));
    render::get()->worldcam();
    glLightfv(GL_LIGHT1, GL_POSITION, render::get()->getLight()->getPosition()->get());

    glPointSize(5);
    glBegin(GL_POINTS);
        glVertex3fv(render::get()->getLight()->getPosition()->get());
    glEnd();
*/
    render::shading(s);
}// paintGL

void GLWidget::initializeGL(){
    glClearColor(0,0,0,1);

/*
    LEC::chair(&s);
    LEC::trash(&s);
    LEC::armario(&s);

    s->shlist().at(0)->transform(transform::translate(Vertex(1.5,0,0)));
    s->shlist().at(1)->transform(transform::translate(Vertex(0,0,1.5)));

*/

    LEC::buildLEC(&s);
/*
    s->setCamera(new Camera(new Vertex(0,18,0),
                                  new Vertex(0,0,0),
                                  new Vertex(0,0,5)));
*/
    s->setCamera(new Camera(new Vertex(0,1,5.9),
                                  new Vertex(0,0,0),
                                  new Vertex(0,3,0)));

    s->setLight(new Light(new Vertex(0,3,0),
                                new Vertex(0.4, 0.4, 0.4),
                                new Vertex(1,1,1),
                                new Vertex(1,1,1)));

    Light* l = s->getLight();
    render::send(s);
    render::get()->worldcam();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();   
    glFrustum(-0.5,0.5,-0.5,0.5,1, 50);
    float c = 10;
    //glOrtho(-c,c,-c,c,-50, 50);

    glMatrixMode(GL_MODELVIEW);

    glLightfv(GL_LIGHT1, GL_AMBIENT, l->getAmbient()->get());
    glLightfv(GL_LIGHT1, GL_DIFFUSE, l->getDifuse()->get());
    glLightfv(GL_LIGHT1, GL_SPECULAR, l->getDifuse()->get());
    glLightfv(GL_LIGHT1, GL_POSITION, l->getPosition()->get());

    glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0);
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.1);
    glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.5);

    glViewport(0,0,700,700);

    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT1);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
}// initializeGL
