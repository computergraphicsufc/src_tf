#include "include/primitives/cube.h"
#include "include/components/vertex_component.h"

Cube::Cube(std::string name, float x):
    Object(name)
{
    float c = x/2;
    addVertex(new Vertex(c, -c, c)); addVertex(new Vertex(c, -c, -c));
    addVertex(new Vertex(-c, -c, -c)); addVertex(new Vertex(-c, -c, c));

    addVertex(new Vertex(c, c, c)); addVertex(new Vertex(c, c, -c));
    addVertex(new Vertex(-c, c, -c)); addVertex(new Vertex(-c, c, c));
/*
    addFace(3,0,7); addFace(0,4,7);
    addFace(0,5,4); addFace(0,1,5);
    addFace(2,3,6); addFace(3,7,6);
    addFace(1,2,5); addFace(2,6,5);
    addFace(0,3,1); addFace(0,2,1);
    addFace(4,5,6); addFace(4,6,7);
*/
    addFace(0,1,5); addFace(0,5,4);
    addFace(1,2,6); addFace(1,6,5);
    addFace(2,3,7); addFace(2,7,6);
    addFace(3,0,4); addFace(3,4,7);
    addFace(0,3,2); addFace(0,2,1);
    addFace(4,5,6); addFace(4,6,7);

    Type = CUBE;
}

float Cube::getSphereBound(){
    return vertex_fun::length(vertex_fun::minus(*vat(6),*vat(0)))/2;
}// getSphereBound

Vertex Cube::getSphereCenter(){
    return vertex_fun::div(vertex_fun::sum(*vat(6),*vat(0)),2);
}
