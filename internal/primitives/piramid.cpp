#include "include/primitives/piramid.h"

#include "include/components/vertex_component.h"

Piramid::Piramid(std::string name, float height, float base):
    Object(name){

    float h = height/2.0;
    float b = base/2.0;

    addVertex(new Vertex(b,-h,b)); addVertex(new Vertex(b,-h,-b)); addVertex(new Vertex(-b,-h,-b));
    addVertex(new Vertex(-b,-h,b)); addVertex(new Vertex(0,h,0));

    addFace(0,1,4); addFace(1,2,4); addFace(2,3,4); addFace(3,0,4);
    addFace(0,1,2); addFace(0,2,3);

    setMaterial(new Vertex(0, 0, 0.5),
                    new Vertex(0, 0, 1),
                    new Vertex(0, 0, 1),
                    1,0,0);
    Type = PIRAMID;
}

float Piramid::getSphereBound(){
    Vertex c = getSphereCenter();
    float b = vertex_fun::length(vertex_fun::minus(*vat(4), c));
    return b;
}

Vertex Piramid::getSphereCenter(){
    Vertex c = vertex_fun::sum(*vat(0), vertex_fun::sum(*vat(1), vertex_fun::sum(*vat(2),vertex_fun::sum(*vat(3),*vat(4)))));
    c = vertex_fun::mult(0.2, c);
    return c;
}
