#include "include/input-output/writer.h"

#include <iostream>
#include <fstream>

void Writer::write(Scene* s, std::string dest){
    std::ofstream file;

    file.open(dest);
    file << *s;
    file.close();
}
