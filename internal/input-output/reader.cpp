#include "include/input-output/reader.h"

#include <iostream>
#include <fstream>

#define X std::cout << "b" << std::endl;

// -------------------------------------------------------- //

Scene* Reader::read(std::string s){
    Scene* scene = new Scene();
    std::string temp;

    std::ifstream file;
    file.open(s);

    if(file.is_open()){
        while(!file.eof()){
            getline(file,temp);
            if(temp == "o"){
                std::string toObject = "";
                while(temp != "?o"){
                    getline(file, temp);
                    toObject = toObject + temp + "\n";
                }// while
                readObject(toObject);
            }// object
        }// while
    } else {
        std::cout << "File not Found" << std::endl;
    }// if-else
    file.close();
}

// -------------------------------------------------------- //

Object* Reader::readObject(std::string s){

    int i = 0;
    int j = 0;

    std::string nome, tipo;
    Material* material;

    // NOME
    while(s[j] != '\n') j++;
    nome = s.substr(i,j);
    j++;
    i = j;

    // TIPO
    while(s[j] != '\n') j++;
    tipo = s.substr(i,j-i);
    j++;
    i = j;


    while(s[j] != '\n') j++;
    if(s.substr(i, j-i) == "m"){
        i = j+1;

        int k;
        while(s.substr(k, j-k) != "?m"){
            j++;
            k = j;
           while(s[j] != '\n')j++;
        }//while

        material = readMaterial(s.substr(i, j-i));
    }// if
}// readObject

// -------------------------------------------------------- //

Vertex* Reader::readVertex(std::string s){
    int size = s.length();
    std::string tmp;

    float temp[3];
    int j = 0;
    int i = 0;

    while(s.substr(i, j-i) != "?v"){
        i = j;
        while(s[j] != '\n') j++;
        tmp = s.substr(i,j-i);
        if(tmp != "?v"){
            //associação de valores aqui
        }
        j++;

    }//for

    return new Vertex(0,0,0);
}//readVertex

// -------------------------------------------------------- //

Material* Reader::readMaterial(std::string s){
    int i = 0, j = 0;

//    while(s.substr(j, j-i)){
        i = j;
        while(s[j] != '\n')j++;

        if(s.substr(i, j-i) == "v"){
            i = j+1;

            int k = j;
            while(s.substr(k, j-k) != "?v"){
                j++;
                k = j;
               while(s[j] != '\n')j++;
            }//while
            readVertex(s.substr(i,j-i));
        }// if
//    }// while



}// readMaterial

// -------------------------------------------------------- //
