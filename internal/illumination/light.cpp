#include "include/illumination/light.h"

#include "include/components/matrix_component.h"

//============================================================//
//============================================================//
//============================================================//

// CONSTRUCTOR

Light::Light(Vertex* pos,Vertex* amb,Vertex* dif,Vertex* spec){
    position = pos;
    ambient = amb;
    difuse = dif;
    specular = spec;

}// constructor

// DESTRUCTOR

Light::~Light(){
    std::cout << "WTF" << std::endl;
    delete position;
    delete ambient;
    delete difuse;
    delete specular;
}

//============================================================//
//============================================================//
//============================================================//

// setters
void Light::setAmbient(Vertex* amb){
    ambient = amb;
}// setAmbient

//------------------------------------------------------------//


void Light::setDifuse(Vertex* dif){
    difuse = dif;
}// setDifuse

//------------------------------------------------------------//


void Light::setSpecular(Vertex* spec){
    specular = spec;
}// setSpecular

//------------------------------------------------------------//


void Light::setPosition(Vertex* pos){
    position = pos;
}// setPosition

//============================================================//
//============================================================//
//============================================================//

// getters
Vertex* Light::getAmbient(){
    return ambient;
}// getAmbient

//------------------------------------------------------------//


Vertex* Light::getDifuse(){
    return difuse;
}// getDifuse

//------------------------------------------------------------//


Vertex* Light::getSpecular(){
    return specular;
}// getSpecular

//------------------------------------------------------------//


Vertex* Light::getPosition(){
    return position;
}// getPosition

//============================================================//
//============================================================//
//============================================================//

void Light::transform(Matrix m){
    Vertex v = matrix_fun::mult(m, *position);
    position->set(v.get(0), 0);
    position->set(v.get(1), 1);
    position->set(v.get(2), 2);
}
