#include "include/observer/camera.h"

#include "include/components/matrix_component.h"

//------------------------------------------------------------//

Camera::Camera(Vertex* p,Vertex* l,Vertex* v)
{
    position = p;
    viewUp = v;
    lookAt = l;
    screen = new Screenshot(1,1,1,SIZE,SIZE);

    update();
}// constructor

//------------------------------------------------------------//

Vertex* Camera::getPosition(){
    return position;
}// getPosition

//------------------------------------------------------------//

Vertex* Camera::getLookAt(){
    return lookAt;
}// getLookAt

//------------------------------------------------------------//

Vertex* Camera::getVup(){
    return viewUp;
}// getVup

//------------------------------------------------------------//

Vertex* Camera::getIc(){
    return ic;
}// getIc

//------------------------------------------------------------//

Vertex* Camera::getJc(){
    return jc;
}// getJc

//------------------------------------------------------------//

Vertex* Camera::getKc(){
    return kc;
}// getKc

//------------------------------------------------------------//

Matrix* Camera::getWC(){
    return wc;
}// getWC

//------------------------------------------------------------//

Matrix* Camera::getCW(){
    return cw;
}

Matrix *Camera::getProjection()
{
    return projection;
}// getCW

//------------------------------------------------------------//

void Camera::convert(Object** o){
    Object* o2 = *o;
    int sv = o2->vSize();
    Vertex* u;

    for(int index = 0 ; index < sv ; index++){
        u = o2->vat(index);
        Vertex v = matrix_fun::mult(*wc, *u);

        u->set(v.get(0), 0);
        u->set(v.get(1), 1);
        u->set(v.get(2), 2);
    }// for
    *o = o2;
}

//------------------------------------------------------------//

void Camera::toWorld(Object ** o)
{
    Object* o2 = *o;
    int sv = o2->vSize();
    Vertex* u;

    for(int index = 0 ; index < sv ; index++){
        u = o2->vat(index);
        Vertex v = matrix_fun::mult(*cw, *u);

        u->set(v.get(0), 0);
        u->set(v.get(1), 1);
        u->set(v.get(2), 2);
    }// for
    *o = o2;
}// toWorld

//------------------------------------------------------------//

void Camera::update(){
    Vertex temp_kc = vertex_fun::norm(vertex_fun::minus(*position, *lookAt));

    // Create coordinate system
        kc = new Vertex(temp_kc.get(0),temp_kc.get(1),temp_kc.get(2));
        Vertex vup = vertex_fun::minus(*viewUp, *position);
        Vertex temp_ic = vertex_fun::norm(vertex_fun::cross(vup, *kc));
        ic = new Vertex(temp_ic.get(0),temp_ic.get(1),temp_ic.get(2));
        Vertex temp_jc = vertex_fun::cross(*kc, *ic);

        jc = new Vertex(temp_jc.get(0),temp_jc.get(1),temp_jc.get(2));
    // Create coordinate system

    // set matrices
        cw = new Matrix(ic->get(0),jc->get(0),kc->get(0), position->get(0),
                        ic->get(1),jc->get(1),kc->get(1), position->get(1),
                        ic->get(2),jc->get(2),kc->get(2), position->get(2),
                        0,0,0,1);

        wc = new Matrix(ic->get(0), ic->get(1), ic->get(2), -vertex_fun::dot(*ic, *position),
                        jc->get(0), jc->get(1), jc->get(2), -vertex_fun::dot(*jc, *position),
                        kc->get(0), kc->get(1), kc->get(2), -vertex_fun::dot(*kc, *position),
                        0,0,0,1);
    // set matrices
}// update

//------------------------------------------------------------//

Screenshot* Camera::getScreenshot(){
    return screen;
}

//------------------------------------------------------------//

void Camera::project(Object** o){
    float alpha, d = -screen->getDistance();
    Object* o2 = *o;
    int sv = o2->vSize();
    Vertex* point;

    for(int i = 0 ; i < sv ; i++){
        point = o2->vat(i);

        alpha = d/point->get(2);

        point->set(point->get(0)*alpha, 0);
        point->set(point->get(1)*alpha, 1);
        point->set(point->get(2)*alpha, 2);
    }// for
}// project

//------------------------------------------------------------//

Camera::~Camera(){
    delete position; delete viewUp; delete lookAt;
    delete ic; delete jc; delete kc;
    delete wc; delete cw; delete projection;
    delete screen;
}

//------------------------------------------------------------//

void Camera::setScreenshot(Screenshot *s){
    delete screen;
    screen = s;
}

//------------------------------------------------------------//

void Camera::transform(Matrix m){
    Vertex v = matrix_fun::mult(m, *position);
    Vertex u = matrix_fun::mult(m, *viewUp);
    position->set(v.get(0), 0);
    position->set(v.get(1), 1);
    position->set(v.get(2), 2);

    viewUp->set(u.get(0), 0);
    viewUp->set(u.get(1), 1);
    viewUp->set(u.get(2), 2);

    update();
}

//------------------------------------------------------------//

void Camera::setPosition(Vertex v){
    delete position;
    position = new Vertex(v.get(0),v.get(1),v.get(2));
}

void Camera::setViewUp(Vertex v){
    delete viewUp;
    viewUp = new Vertex(v.get(0),v.get(1),v.get(2));
}

void Camera::setLookAt(Vertex v){
    delete lookAt;
    lookAt = new Vertex(v.get(0),v.get(1),v.get(2));
}
