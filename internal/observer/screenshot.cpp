#include "include/observer/screenshot.h"

Screenshot::Screenshot(float d , float sw, float sh,
                       float ww, float wh)
{
    distance = d;
    screenW = sw;
    screenH = sh;

    windowW = ww;
    windowH = wh;

    matrix = new MatrixColor(wh,ww);
}// constructor

// --------------------------------------------------------------------------------- //

float Screenshot::getDistance(){
    return distance;
}

// --------------------------------------------------------------------------------- //

float Screenshot::getScreenWidth(){
    return screenW;
}

// --------------------------------------------------------------------------------- //

float Screenshot::getScreenHeight(){
    return screenH;
}

// --------------------------------------------------------------------------------- //

int Screenshot::getWindowWidth(){
    return windowW;
}

// --------------------------------------------------------------------------------- //

int Screenshot::getWindowHeight(){
    return windowH;
}

// --------------------------------------------------------------------------------- //

MatrixColor* Screenshot::getMatrixColor(){
    return matrix;
}

// --------------------------------------------------------------------------------- //

Screenshot::~Screenshot(){
    delete matrix;
}// destructor

// --------------------------------------------------------------------------------- //
