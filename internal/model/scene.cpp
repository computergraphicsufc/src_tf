#include "include/model/scene.h"

#include "include/components/matrix_component.h"
#include "include/components/transform_component.h"
#include "include/model/shape_n.h"

#define STEP_W 1
#define STEP_G  10

Scene::Scene(){
    camera = new Camera(new Vertex(1,1,1),
                        new Vertex(0,0,0),
                        new Vertex(0,1,0));

    light = new Light(new Vertex(1,1,0),
                      new Vertex(0.2,0.2,0.2),
                      new Vertex(1,1,1),
                      new Vertex(1,1,1));

    group = new Shape_n("main");
}

void Scene::newObject(Object* o){
    olist.push_back(o);
}

void Scene::constructTree(){
    delete group;
    group = new Shape_n("main");

    if(!olist.empty())
        group->add(olist.at(0));

    int size = olist.size();
    for(int i = 1 ; i < size ; i++){
        bool grouped = groupby(olist.at(i), group);
        if(!grouped){
            Shape *g;
            float* min = new float; *min = INFINITY;
            constructNode(olist.at(i), group, &g, min);
            delete min;

            Shape *ng = new Shape_n("son");
            ng->add(olist.at(i));
            g->add(ng);
        }// if
    }//for
}// constructTree

void Scene::attach(Shape* s){
    glist.push_back(s);
}

void Scene::detach(int){

}

bool Scene::groupby(Object* o, Shape* s){
    Vertex shapeC = s->getSphereCenter();
    Vertex objectC = o->getSphereCenter();
    bool test = false;

    float l = vertex_fun::length(vertex_fun::minus(shapeC, objectC));
    if(l < distance){
        s->add(o);
        return true;
    } else {
        int size = s->size();

        for(int i = 0 ; i < size ; i++)
            if(s->get(i)->type() == SHAPE_N)
                test = groupby(o, s->get(i));
    }// if-else

    if(test) return true;
    return false;
}// groupby

void Scene::constructNode(Object* o, Shape* s, Shape** parent, float* min){
    Vertex shapeC = s->getSphereCenter();
    Vertex objectC = o->getSphereCenter();
    float l = vertex_fun::length(vertex_fun::minus(shapeC, objectC));

    if(l < *min){
        *min = l;
        *parent = s;
    }

    int size = s->size();
    for(int i = 0 ; i < size ; i++)
        if(s->get(i)->type() == SHAPE_N)
            constructNode(o, s->get(i), parent, min);
}// constructNode

void Scene::delObject(Object* o){}

void Scene::setCamera(Camera* c){
    if(!camera) delete camera;
    camera = c;
}

void Scene::setLight(Light* l){
    if(!light) delete light;
    light = l;
}

void Scene::worldcam(){
    int size = olist.size();
    for(int i = 0 ; i < size ; i++)
        olist.at(i)->transform(*camera->getWC());
    light->transform(*camera->getWC());
}// worldcam

void Scene::camworld(){
    int size = olist.size();
    for(int i = 0 ; i < size ; i++)
        olist.at(i)->transform(*camera->getCW());
    light->transform(*camera->getCW());
}// camworld


// funcão com problema: quando a posição é negativa e ele tenta andar pra frente, ele vai pra trás
void Scene::walk(int slot){
    int step;

    Vertex direction;
    if(slot == FRONT){
        step = -STEP_W;
        direction = *camera->getKc();
    }

    if(slot == BACK){
        step = STEP_W;
        direction = *camera->getKc();
    }
    if(slot == LEFT){
        step = STEP_W;
        direction = *camera->getIc();
    }
    if(slot == RIGHT){
        step = -STEP_W;
        direction = *camera->getIc();
    }

    Vertex newP = vertex_fun::sum(*camera->getPosition(), vertex_fun::mult(step, direction));
    camera->setPosition(newP);
    Vertex look = vertex_fun::sum(*camera->getLookAt(), vertex_fun::mult(step, direction));
    camera->setLookAt(look);
    Vertex vusp = vertex_fun::sum(*camera->getVup(), vertex_fun::mult(step, direction));
    camera->setViewUp(vusp);

    camera->update();
}

void Scene::girate(int slot){

    int step;
    Vertex direction;

    if(slot == G_LEFT){
        step = -STEP_G;
        direction = *camera->getJc();
    }
    if(slot == G_RIGHT){
        step = STEP_G;
        direction = *camera->getJc();
    }
    if(slot == G_UP){
        step = -STEP_G;
        direction = *camera->getIc();
    }
    if(slot == G_DOWN){
        step = STEP_G;
        direction = *camera->getIc();
    }

    Vertex pos = *camera->getPosition();
    Vertex look = vertex_fun::minus(*camera->getLookAt(), pos);
    Vertex vup = vertex_fun::minus(*camera->getVup(), pos);


    Matrix m = transform::rotate(-step, direction);
    look = matrix_fun::mult(m, look);
    vup = matrix_fun::mult(m, vup);

    camera->setLookAt(vertex_fun::sum(look, pos));
    camera->setViewUp(vertex_fun::sum(vup, pos));

    camera->update();
}

Shape* Scene::getGroup(){
    return group;
}

Object* Scene::duplicateObject(int index){

}

Shape* Scene::duplicateShape(int index){

}


std::vector<Object*> Scene::oblist(){
    return olist;
}

std::vector<Shape*> Scene::shlist(){
    return glist;
}

Camera* Scene::getCamera(){
    return camera;
}

Light* Scene::getLight(){
    return light;
}

void Scene::constructGroup(){
    renderglist.clear();

    for(int i = 0 ; i < olist.size() ; i++)
        olist.at(i)->setInGroup(false);
    for(int i = 0 ; i < glist.size() ; i++){
        constructGroupSearch(glist.at(i));
        renderglist.push_back(glist.at(i));
    }// for

    for(int i = 0 ; i < olist.size() ; i++){
        if(!olist.at(i)->inGroup())
            renderglist.push_back(olist.at(i));
            olist.at(i)->setInGroup(true);
    }// for

    for(int i = 0 ; i < renderglist.size() ; i++){
        std::cout << "group ================================== \n";
        renderglist.at(i)->print(0);
    }
}

void Scene::constructGroupSearch(Shape* s){
    int size = s->size();

    for(int i = 0 ; i < size ; i++){
        if(s->get(i)->type() >= OBJECT){
            s->get(i)->setInGroup(true);
        }
        if(s->get(i)->type() == SHAPE_N)
            constructGroupSearch(s->get(i));
    }
}// groupSearch
