#include "include/model/obj.h"

Obj::Obj(){
    Type = OBJ;
}// constructor

int Obj::type(){
    return Type;
}// type

void Obj::debug(){
    std::cout << "Obj" << std::endl;
}

int Obj::size(){
    return 0;
}

Obj* Obj::get(int a){
    return this;
}

std::string Obj::getName(){
    return "obj";
}
