#include "include/model/vertex.h"

Vertex::Vertex(float x1, float y1, float z1)
{
    c[0] = x1;
    c[1] = y1;
    c[2] = z1;
    c[3] = 1;
}

void Vertex::set(float coord, int index)
{
    c[index%4] = coord;
}

float Vertex::get(int index)
{
    return c[index%4];
}// constructor

float* Vertex::get(){
    return c;
}

Vertex* Vertex::duplicate(){
    return new Vertex(c[0],c[1],c[2]);
}

Vertex::Vertex():Vertex(0,0,0){}
