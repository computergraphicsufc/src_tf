#include "include/model/matrix.h"

Matrix::Matrix(float e1,float e2,float e3,float e4,float e5,float e6,float e7,float e8,
               float e9,float e10,float e11,float e12,float e13,float e14,float e15,float e16){
    mat[0][0] = e1; mat[0][1] = e2; mat[0][2] = e3; mat[0][3] = e4;
    mat[1][0] = e5; mat[1][1] = e6; mat[1][2] = e7; mat[1][3] = e8;
    mat[2][0] = e9; mat[2][1] = e10; mat[2][2] = e11; mat[2][3] = e12;
    mat[3][0] = e13; mat[3][1] = e14; mat[3][2] = e15; mat[3][3] = e16;
}// constructor

//------------------------------------------------------------//

Matrix::Matrix():
    Matrix(1,0,0,0,
           0,1,0,0,
           0,0,1,0,
           0,0,0,1)
{}

//------------------------------------------------------------//

float Matrix::get(int i1, int i2){
    return mat[i1][i2];
}// get

//------------------------------------------------------------//

void Matrix::set(int i1, int i2, float val){
    mat[i1][i2] = val;
}// set

//------------------------------------------------------------//

Matrix matrix::identity()
{
    return Matrix(1,0,0,0,
                  0,1,0,0,
                  0,0,1,0,
                  0,0,0,1);
}//
