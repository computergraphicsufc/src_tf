#include "include/model/obj_node.h"

Obj_node::Obj_node()
{
    Type = OBJ_NODE;
}

void Obj_node::add(Obj* obj){
    group.push_back(obj);
}// add

Obj* Obj_node::get(int a){
    return group.at(a);
}// get

void Obj_node::debug(){
    std::cout << "Obj_node" << std::endl;
}

int Obj_node::size(){
    return group.size();
}
