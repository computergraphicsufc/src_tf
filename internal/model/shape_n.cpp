#include "include/model/shape_n.h"
#include "include/components/vertex_component.h"

Shape_n::Shape_n(std::string name):
    Shape(name)
{
    Type = SHAPE_N;
}

void Shape_n::add(Shape* s){
    group.push_back(s);
}

Shape* Shape_n::get(int index){
    return group.at(index);
}

Shape* Shape_n::get(std::string){
    return nullptr;
}

int Shape_n::type(){
    return Type;
}// type

std::string Shape_n::getName(){
    return name;
}// getName

void Shape_n::transform(Matrix m){
    int size = group.size();
    for(int i = 0 ; i < size ; i++)
        group.at(i)->transform(m);
}// transform

float Shape_n::getSphereBound(){
    int size = group.size();
    float max = 0, maxBound = 0;
    float temp;
    Vertex a = getSphereCenter();
    Vertex vtemp;

    for(int i = 0 ; i < size ; i++){
        temp = group.at(i)->getSphereBound();
        vtemp = group.at(i)->getSphereCenter();
        if(temp > max) max = temp;

        temp = vertex_fun::length(vertex_fun::minus(vtemp, a));
        if(temp > maxBound) maxBound = temp;
    }// for

    return max+maxBound;
}// getSphereBound

Vertex Shape_n::getSphereCenter(){
    int size = group.size();
    Vertex a(0,0,0);

    for(int i = 0 ; i < size ; i++)
        a = vertex_fun::sum(a, group.at(i)->getSphereCenter());

    a = vertex_fun::div(a, size);
    return a;
}// getSphereCenter

int Shape_n::size(){
    return group.size();
}

void Shape_n::print(int role){
    for(int i = 0 ; i < role ; i++)
        std::cout << "  ";
    std::cout << "nó intermediario: " << getSphereCenter() << " : " << getSphereBound() << "\n";

    int size = group.size();
    for(int i = 0 ; i < size ; i++)
        group.at(i)->print(role+1);
}

Shape* Shape_n::duplicate(){

}
