#include "include/model/face.h"

Face::Face(Vertex* v1,Vertex* v2,Vertex* v3)
{
    f[0] = v1;
    f[1] = v2;
    f[2] = v3;
}// constructor

//------------------------------------------------------------//

Vertex* Face::get(int index)
{
    return f[index%3];
}

Face::~Face()
{
    for(int i = 0 ; i < 3 ; i++)
        if(!f[i])
            delete f[i];
}// get

//------------------------------------------------------------//

void Face::set(Vertex* u, int index)
{
    delete f[index%3];
    f[index%3] = u;
}// set

//------------------------------------------------------------//
