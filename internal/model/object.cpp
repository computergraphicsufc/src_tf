#include "include/model/object.h"
#include "include/components/matrix_component.h"
#include "include/components/vertex_component.h"

Object::Object(std::string s):
    Shape(s)
{
    material = new Material(new Vertex(0.2,0,0),
                            new Vertex(1,0,0),
                            new Vertex(1,0,0),
                            1,0,1);
    material->setDifraction(1);
    Type = OBJECT;
}

// ---------------------------------------------- //

std::string Object::getName(){
    return name;
}

// ---------------------------------------------- //

int Object::type(){
    return Type;
}

// ---------------------------------------------- //

void Object::addVertex(Vertex* u){
    vList.push_back(u);
}// addVertex

//------------------------------------------------------------//

void Object::addFace(int v1, int v2, int v3){
    fList.push_back(new Face(vList.at(v1),
                             vList.at(v2),
                             vList.at(v3)));
    int* array = new int[3];
    array[0] = v1;
    array[1] = v2;
    array[2] = v3;
    fpointers.push_back(array);
} // addFace

//------------------------------------------------------------//

void Object::setMaterial(Vertex * a, Vertex * d, Vertex * s, float sh, float r, float t)
{
    delete material;
    material = new Material(a,d,s,sh,r,t);
}// setMaterial

//------------------------------------------------------------//

Material *Object::getMaterial()
{
    return material;
}// getMaterial

//------------------------------------------------------------//

int Object::fSize(){
    return fList.size();
}// fSize

//------------------------------------------------------------//

int Object::vSize(){
    return vList.size();
}// vSize

//------------------------------------------------------------//

Vertex* Object::vat(int index){
    return vList.at(index);
}// vat

//------------------------------------------------------------//

Face* Object::fat(int index){
    return fList.at(index);
}// fat

//------------------------------------------------------------//

void Object::transform(Matrix m){
    int size = vList.size();
    Vertex *u, v;
    for(int i = 0 ; i < size ; i++){
        u = vList.at(i);
        v = matrix_fun::mult(m, *u);

        u->set(v.get(0),0);
        u->set(v.get(1),1);
        u->set(v.get(2),2);
    }//for
}//transform

void Object::print(int role){
    for(int i = 0 ; i < role ; i++)
        std::cout << "  ";
    std::cout << name << ": " <<  getSphereCenter() << " : " << getSphereBound() << std::endl;
}

//------------------------------------------------------------//

Object* Object::duplicate(){
    Object* duplicata = new Object(name + "-d");
    Vertex* v;
    int size = vList.size();

    for(int i = 0 ; i < size ; i++){
        v = vList.at(i);
        duplicata->addVertex(v->duplicate());
    }// for

    size = fpointers.size();
    int* fp;

    for(int i = 0 ; i < size ; i++){
        fp = fpointers.at(i);
        duplicata->addFace(fp[0],fp[1],fp[2]);
    }// for

    duplicata->setMaterial(material->getAmbient()->duplicate(),
                           material->getDifuse()->duplicate(),
                           material->getSpecular()->duplicate(),
                           material->getShininess(),
                           material->getTransparency(),
                           material->getReflectivity());
    return duplicata;
}// duplicate


// FUNÇÕES DESNECESSARIAS
Object::~Object()
{

}

void Object::add(Shape *){}
Shape* Object::get(std::string){return NULL; }
Shape* Object::get(int){ return NULL;}
int Object::size(){return 0;}

float Object::getSphereBound(){
    Vertex v = getSphereCenter();
    return vertex_fun::length(vertex_fun::minus(v, *vList.at(0)));
}

Vertex Object::getSphereCenter(){
    Vertex v(0,0,0);
    int size = vList.size();
    for(int i = 0 ; i < size ; i++)
        v = vertex_fun::sum(v, *vList.at(i));
    v = vertex_fun::div(v, size);
    return v;
}

// FUNÇÕES DESNECESSARIAS
