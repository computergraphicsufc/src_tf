#include "include/model/matrixcolor.h"

MatrixColor::MatrixColor(int rows, int columns)
{
    // set member variables
        column = columns;
        row = rows;
    // set member variables

    // create matrix with default value Vertex(0,0,0)
    matrix = new Vertex**[row];
    for(int index = 0 ; index < row ; index++){
        matrix[index] = new Vertex*[column];
        for(int index2 = 0 ; index2 < column ; index2++){
            matrix[index][index2] = new Vertex(0,0,0);
        }// for - 2
    }// for - 1
    // create matrix with default value Vertex(0,0,0)
}// constructor

//------------------------------------------------------------//

Vertex* MatrixColor::get(int r, int c){
    return matrix[r][c];
}// get

//------------------------------------------------------------//

void MatrixColor::set(Vertex* u, int r, int c){
    delete matrix[r][c];
    matrix[r][c] = u;
}// set

//------------------------------------------------------------//

void MatrixColor::setAll(float r, float g, float b){
    for(int i = 0 ; i < row ; i++){
        for(int j = 0 ; j < column ; j++){
            delete matrix[i][j];
            matrix[i][j] = new Vertex(r,g,b);
        }// for-2
    }// for-1
}

//------------------------------------------------------------//

MatrixColor::~MatrixColor(){
    for(int i = 0 ; i < row ; i++){
        for(int j = 0 ; j < column ; j++){
            delete matrix[i][j];
        }// for-2
        delete matrix[i];
    }// for-1

    // for debug
    //std::cout << "Matrix removed" << std::endl;
    delete matrix;
}// destructor

//------------------------------------------------------------//

int MatrixColor::columns(){
    return column;
}// columns

//------------------------------------------------------------//

int MatrixColor::rows(){
    return row;
}// rows

//------------------------------------------------------------//
