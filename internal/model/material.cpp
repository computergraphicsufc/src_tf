#include "include/model/material.h"

//------------------------------------------------------------//

Material::Material(Vertex* a, Vertex* d,
                   Vertex* s, float sh, float ref, float trans)
{
    ambient = a;
    difuse = d;
    specular = s;
    shininess = sh;
    reflectivity = ref;
    transparency = trans;
}// constructor

//------------------------------------------------------------//

Vertex* Material::getAmbient(){
    return ambient;
}// getAbient

//------------------------------------------------------------//

Vertex* Material::getDifuse(){
    return difuse;
}// getDifuse

//------------------------------------------------------------//

Vertex* Material::getSpecular(){
    return specular;
}// getSpecular

//------------------------------------------------------------//

float Material::getShininess(){
    return shininess;
}// getShininess

//------------------------------------------------------------//

Material::~Material(){
    delete ambient;
    delete difuse;
    delete specular;
} // destructor

//------------------------------------------------------------//

float Material::getReflectivity(){
    return reflectivity;
}// getReflectivity

//------------------------------------------------------------//

float Material::getTransparency(){
    return transparency;
}// getTransparency

//------------------------------------------------------------//

float Material::getDifraction(){
    return difraction;
}

//------------------------------------------------------------//

void Material::setDifraction(float a){
    difraction = a;
}
