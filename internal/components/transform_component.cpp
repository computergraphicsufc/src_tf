#include "include/components/transform_component.h"

#include "include/components/matrix_component.h"
#include "include/components/vertex_component.h"
#include <cmath>

#define PI 3.1415926535

// Transform Functions
Matrix transform::rotate(float angle, char axis){
    float radius = (angle *3.1415926535)/180.0;
    Matrix m;

    switch(axis){
        case 'x':
            m = Matrix(1,0,0,0,
                       0,cos(radius),-sin(radius),0,
                       0,sin(radius),cos(radius),0,
                       0,0,0,1);
            break;
        case 'y':
            m = Matrix(cos(radius),0,sin(radius),0,
                           0,1,0,0,
                           -sin(radius),0,cos(radius),0,
                           0,0,0,1);
            break;
        case 'z':
            m = Matrix(cos(radius),-sin(radius),0,0,
                           sin(radius),cos(radius),0,0,
                           0,0,1,0,
                           0,0,0,1);
            break;
    }//switch
    return m;

} // transform prototype

Matrix transform::translate(Vertex t){
    return Matrix(1,0,0,t.get(0),
                  0,1,0,t.get(1),
                  0,0,1,t.get(2),
                  0,0,0,1);
}// translate

Matrix transform::scale(Vertex scale, Vertex point){
    return Matrix(scale.get(0),0,0,(1 - scale.get(0))*point.get(0),
                  0,scale.get(1),0,(1 - scale.get(1))*point.get(1),
                  0,0,scale.get(2),(1 - scale.get(2))*point.get(2),
                  0,0,0,1);
}// scale

Matrix transform::sheer(char axis1, char axis2, float angle){
    Matrix m;
    float radius = (angle *3.1415926535)/180.0;
    m.set(axis1-120, axis2-120, tan(radius));
    return m;
}// sheer

Matrix transform::mirror(Vertex n){
    float a = n.get(0);
    float b = n.get(1);
    float c = n.get(2);
    Matrix m = Matrix((1 - 2*a*a), -2*a*b, -2*a*c, 0,
                           -2*b*a, (1 - 2*b*b), 2*b*c, 0,
                           -2*c*a, -2*c*b, (1-2*c*c), 0,
                           0,0,0,1);
    return m;
}// mirror

/*
Matrix transform::rotate(float ang, Vertex v){
    ang = ang/2; // o quatérnio usa metade do ângulo
    float x = v.get(0);
    float y = v.get(1);
    float z = v.get(2);

    float n = std::sqrt( x*x + y*y + z*z ); // norma do eixo
    x /= n;
    y /= n;
    z /= n;

    x *= std::sin( ang );
    y *= std::sin( ang );
    z *= std::sin( ang );
    float w = std::cos( ang );

    return Matrix(w*w + x*x - y*y - z*z, 2*x*y - 2*w*z, 2*x*z + 2*w*y, 0,
                  2*x*y + 2*w*z,w*w - x*x + y*y - z*z,2*y*z - 2*w*z, 0,
                  2*x*z - 2*w*y,2*y*z + 2*w*z, w*w - x*x - y*y + z*z, 0,
                  0,0,0,1); // linha por linha
}
*/

Matrix transform::rotate(float ang, Vertex v){
    v = vertex_fun::norm(v);

    float a = v.get(0);
    float b = v.get(1);
    float c = v.get(2); // (a,b,c)
    float d = 1; // d = norma de v = 1

    float angx = acos(c/d);
    float angy = acos(d);

    Matrix mx = rotate(angx*(180/PI), 'x');
    Matrix myi = rotate(-angy*(180/PI), 'y');
    Matrix mz = rotate(ang , 'z');
    Matrix my = rotate(angy*(180/PI), 'y');
    Matrix mxi = rotate(-angx*(180/PI), 'x');

    Matrix total = matrix_fun::mult(myi, mx);
    total = matrix_fun::mult(mz, total);
    total = matrix_fun::mult(my, total);
    total = matrix_fun::mult(mxi, total);

    std::cout << angx*(180/PI) << std::endl;
    std::cout << angy*(180/PI) << std::endl;
    std::cout << ang << std::endl;

    /*
     * cosx = c/d
     * senx = b/d
     *
     * cosy = d
     * seny = -a
    */
    return total;
}// rotate

