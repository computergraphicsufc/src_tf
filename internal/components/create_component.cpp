#include "include/components/create_component.h"

Object* create::cube(float side){
    // variables
        Object *o2 = new Object("Cube");
        float c = side/2.0;
    // variables
    // create vertex list
        o2->addVertex(new Vertex(c, -c, c)); o2->addVertex(new Vertex(c, -c, -c));
        o2->addVertex(new Vertex(-c, -c, -c)); o2->addVertex(new Vertex(-c, -c, c));
        o2->addVertex(new Vertex(c, c, c)); o2->addVertex(new Vertex(c, c, -c));
        o2->addVertex(new Vertex(-c, c, -c)); o2->addVertex(new Vertex(-c, c, c));
    // create vertex list

    // adding faces
        o2->addFace(0,1,5); o2->addFace(0,5,4); o2->addFace(1,2,6); o2->addFace(1,6,5); o2->addFace(2,3,7); o2->addFace(2,7,6);
        o2->addFace(3,0,4); o2->addFace(3,4,7); o2->addFace(0,3,2); o2->addFace(0,2,1); o2->addFace(4,5,6); o2->addFace(4,6,7);
    // adding faces

    // material pattern
        // already set to red color
    // material pattern

    return o2;
}// cube

Object* create::piramid(float base, float height)
{
    Object *o2 = new Object("Piramid");
    float h = height/2.0;
    float b = base/2.0;

    o2->addVertex(new Vertex(b,-h,b)); o2->addVertex(new Vertex(b,-h,-b)); o2->addVertex(new Vertex(-b,-h,-b));
    o2->addVertex(new Vertex(-b,-h,b)); o2->addVertex(new Vertex(0,h,0));

    o2->addFace(0,1,4); o2->addFace(1,2,4); o2->addFace(2,3,4); o2->addFace(3,0,4);
    o2->addFace(0,1,2); o2->addFace(0,2,3);

    o2->setMaterial(new Vertex(0, 0, 0.5),
                    new Vertex(0, 0, 1),
                    new Vertex(0, 0, 1),
                    1,0,0);
   return o2;
}// piramid
