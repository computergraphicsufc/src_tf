#include "include/components/matrix_component.h"

// Matrix Functions

Vertex matrix_fun::mult(Matrix m, Vertex v){
    float acumulator[4] = {0,0,0,0};

    for(int i1 = 0; i1 < 4 ; i1++)
        for(int i2 = 0 ; i2 < 4 ; i2++)
            acumulator[i1] += m.get(i1,i2)*v.get(i2);
    return Vertex(acumulator[0],acumulator[1],acumulator[2]);
}// mult

//------------------------------------------------------------//

Matrix matrix_fun::mult(Matrix m, Matrix op){
    Matrix resultado = matrix::identity();
    float acumulator = 0;
    for(int i1 = 0; i1 < 4 ; i1++){
        for(int i2 = 0 ; i2 < 4 ; i2++){
            for(int i3 = 0 ; i3 < 4 ; i3++){
                acumulator += m.get(i1,i3)*op.get(i3, i2);
            }
            resultado.set(i1, i2, acumulator);
            acumulator = 0;
        }// for-2
    }// for-1
    return resultado;
}// mult

//------------------------------------------------------------//
