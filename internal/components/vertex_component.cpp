#include "include/components/vertex_component.h"

// Vertex Functions

Vertex vertex_fun::sum(Vertex u, Vertex v){
    return Vertex(u.get(0) + v.get(0),
                      u.get(1) + v.get(1),
                      u.get(2) + v.get(2));
}// sum

//------------------------------------------------------------//

Vertex vertex_fun::inverse(Vertex u){
    return Vertex(-u.get(0),
                      -u.get(1),
                      -u.get(2));
}// inverse

//------------------------------------------------------------//

Vertex vertex_fun::minus(Vertex u,Vertex v){
    return Vertex(u.get(0) - v.get(0),
                      u.get(1) - v.get(1),
                      u.get(2) - v.get(2));
}// minus

//------------------------------------------------------------//

Vertex vertex_fun::mult(Vertex u,Vertex v){
    return Vertex(u.get(0) * v.get(0),
                      u.get(1) * v.get(1),
                      u.get(2) * v.get(2));
}// mult

//------------------------------------------------------------//

Vertex vertex_fun::mult(float s, Vertex u){
    return Vertex(s*u.get(0),
                      s*u.get(1),
                      s*u.get(2));
}

//------------------------------------------------------------//

Vertex vertex_fun::div(Vertex u, float s){
    if(s != 0){
        return mult(1.0/s, u);
    }//if
    return Vertex(0,0,0);
}// div

//------------------------------------------------------------//

float vertex_fun::dot(Vertex u,Vertex v){
    return  u.get(0) * v.get(0) +
            u.get(1) * v.get(1) +
            u.get(2) * v.get(2);
}// dot

//------------------------------------------------------------//

float vertex_fun::length(Vertex u){
    return sqrt(dot(u,u));
}// lenght

//------------------------------------------------------------//

Vertex vertex_fun::norm(Vertex u){
    return div(u, length(u));
}// norm

//------------------------------------------------------------//

Vertex vertex_fun::cross(Vertex u,Vertex v){
    return Vertex(u.get(1)*v.get(2) - u.get(2)*v.get(1),
                   u.get(2)*v.get(0) - u.get(0)*v.get(2),
                   u.get(0)*v.get(1) - u.get(1)*v.get(0));
}// cross
