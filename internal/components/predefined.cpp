#include "include/components/predefined.h"
#include "include/primitives/cube.h"
#include "include/components/transform_component.h"
#include "include/model/shape.h"
#include "include/model/shape_n.h"

void construct::room(Scene** s){
    Scene* scene = *s;

    // construção dos objetos
        // chão
        scene->newObject(new Cube("floor1", 1));    //0
        scene->newObject(new Cube("ceil1", 1));     //1

        scene->newObject(new Cube("front1", 1));    //2
        scene->newObject(new Cube("back1", 1));     //3
        scene->newObject(new Cube("right1", 1));    //4
        scene->newObject(new Cube("left1", 1));     //5

        scene->newObject(new Cube("mirror", 1));    //6
        scene->newObject(new Cube("object", 1));    //7

    // construção dos objetos

    // transformação dos objetos
        scene->oblist().at(0)->transform(transform::scale(Vertex(2,0.1,2), Vertex(0,0,0)));
        scene->oblist().at(0)->transform(transform::translate(Vertex(0,-0.05,0)));

        scene->oblist().at(1)->transform(transform::scale(Vertex(2,0.1,2), Vertex(0,0,0)));
        scene->oblist().at(1)->transform(transform::translate(Vertex(0,1.05,0)));

        scene->oblist().at(2)->transform(transform::scale(Vertex(0.1,1,2), Vertex(0,0,0)));
        scene->oblist().at(2)->transform(transform::translate(Vertex(0.95,0.5,0)));

        scene->oblist().at(3)->transform(transform::scale(Vertex(0.1,1,2), Vertex(0,0,0)));
        scene->oblist().at(3)->transform(transform::translate(Vertex(-0.95,0.5,0)));

        scene->oblist().at(4)->transform(transform::scale(Vertex(1.8,1,0.1), Vertex(0,0,0)));
        scene->oblist().at(4)->transform(transform::translate(Vertex(0,0.5,0.95)));

        scene->oblist().at(5)->transform(transform::scale(Vertex(1.8,1,0.1), Vertex(0,0,0)));
        scene->oblist().at(5)->transform(transform::translate(Vertex(0,0.5,-0.95)));

        scene->oblist().at(6)->transform(transform::scale(Vertex(0.1,1,2), Vertex(0,0,0)));
        scene->oblist().at(6)->transform(transform::translate(Vertex(-0.85,0.5,0)));

        scene->oblist().at(7)->transform(transform::scale(Vertex(0.5,0.5,0.5), Vertex(0,0,0)));
    // transformação dos objetos

    // propriedades dos objetos
        scene->oblist().at(2)->setMaterial(new Vertex(0,0.3,0.3), new Vertex(0,1,1), new Vertex(0,1,1), 1, 0, 0);
        scene->oblist().at(6)->setMaterial(new Vertex(0,0,0), new Vertex(0,0,0), new Vertex(0,0,0), 1, 1, 1);
        scene->oblist().at(7)->setMaterial(new Vertex(0.4,0,0.4), new Vertex(1,0,1), new Vertex(1,0,1), 1, 0, 1);
    // propriedades dos objetos

    scene->setCamera(new Camera(new Vertex(0.8,0.75,0),
                                  new Vertex(0,0.5,0),
                                  new Vertex(0,2,0)));

    scene->setLight(new Light(new Vertex(2,2,2),
                                new Vertex(0.4, 0.4, 0.4),
                                new Vertex(1,1,1),
                                new Vertex(1,1,1)));

}
void construct::mirroom(Scene** s){
    Scene* scene = *s;

    scene->newObject(new Cube("mirror1", 1));
    scene->newObject(new Cube("mirror2", 1));
    scene->newObject(new Cube("cube1", 1));
    scene->newObject(new Cube("mirror3", 1));

    scene->oblist().at(0)->transform(transform::scale(Vertex(5,3,0.1), Vertex(0,0,0)));
    scene->oblist().at(0)->transform(transform::rotate(90, 'y'));

    scene->oblist().at(1)->transform(transform::scale(Vertex(5,3,0.1), Vertex(0,0,0)));
    //scene->oblist().at(1)->transform(transform::rotate(-10, 'y'));

    scene->oblist().at(2)->transform(transform::scale(Vertex(0.7,0.7,0.7), Vertex(0,0,0)));
    scene->oblist().at(2)->transform(transform::translate(Vertex(1,0,1)));

    scene->oblist().at(3)->transform(transform::scale(Vertex(5,3,0.1), Vertex(0,0,0)));
    scene->oblist().at(3)->transform(transform::rotate(45, 'y'));
    scene->oblist().at(3)->transform(transform::translate(Vertex(2,-1,2)));

    scene->oblist().at(0)->setMaterial(new Vertex(0.3,0.3,0.3), new Vertex(0,0,0), new Vertex(0,0,0), 1, 1, 1);
    scene->oblist().at(1)->setMaterial(new Vertex(0.3,0.3,0.3), new Vertex(0,0,0), new Vertex(0,0,0), 1, 1, 1);
    scene->oblist().at(2)->setMaterial(new Vertex(0,0,0.4), new Vertex(0,0,1), new Vertex(0,0,0.3), 1, 0, 1);
    scene->oblist().at(3)->setMaterial(new Vertex(0.3,0.3,0.3), new Vertex(0,0,0), new Vertex(0,0,0), 1, 1, 1);
}
void LEC::chair(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("cadeira");

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        Cube* ob = new Cube("assento1", 1);
        ob->transform(transform::scale(Vertex(0.4,0.01,0.4), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0.02,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0,0),
                        new Vertex(0.7,0,0),
                        new Vertex(0.2,0,0),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("assento2", 1);
        ob->transform(transform::scale(Vertex(0.4,0.02,0.4), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0.005,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0,0),
                        new Vertex(0.7,0,0),
                        new Vertex(0.2,0,0),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("assento3", 1);
        ob->transform(transform::scale(Vertex(0.4,0.02,0.4), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,-0.015,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.2,0.2,0.2),
                        new Vertex(0.2,0.2,0.2),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("perna1", 1);
        ob->transform(transform::scale(Vertex(0.05,0.5,0.05), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0.175, -0.275, 0.175)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0,0,0),
                        new Vertex(0.2,0.2,0.2),
                        1,0.1,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("perna2", 1);
        ob->transform(transform::scale(Vertex(0.05,0.5,0.05), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0.175, -0.275, -0.175)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0,0,0),
                        new Vertex(0.2,0.2,0.2),
                        1,0.1,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("perna3", 1);
        ob->transform(transform::scale(Vertex(0.05,0.5,0.05), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(-0.175, -0.275, -0.175)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0,0,0),
                        new Vertex(0.2,0.2,0.2),
                        1,0.1,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("perna4", 1);
        ob->transform(transform::scale(Vertex(0.05,0.5,0.05), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(-0.175, -0.275, 0.175)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0,0,0),
                        new Vertex(0.2,0.2,0.2),
                        1,0.1,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("apoio1", 1);
        ob->transform(transform::scale(Vertex(0.4,0.45,0.01), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0.25,-0.155)));
        // properties
        ob->setMaterial(new Vertex(0.2,0,0),
                        new Vertex(0.6,0,0),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("apoio2", 1);
        ob->transform(transform::scale(Vertex(0.4,0.45,0.02), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0.25,-0.17)));
        // properties
        ob->setMaterial(new Vertex(0.2,0,0),
                        new Vertex(0.6,0,0),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("apoio3", 1);
        ob->transform(transform::scale(Vertex(0.4,0.45,0.02), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0.25,-0.19)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.2,0.2,0.2),
                        new Vertex(0.2,0.2,0.2),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("lateral1", 1);
        ob->transform(transform::scale(Vertex(0.05,0.05,0.35), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0.175,0.2,0.025)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0,0,0),
                        new Vertex(0.2,0.2,0.2),
                        1,0.1,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("lateral2", 1);
        ob->transform(transform::scale(Vertex(0.05,0.05,0.35), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(-0.175,0.2,0.025)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0,0,0),
                        new Vertex(0.2,0.2,0.2),
                        1,0.1,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::table(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("Mesa");

    c->add(new Cube("base", 1));
    c->add(new Cube("perna1", 1));
    c->add(new Cube("perna2", 1));
    c->add(new Cube("perna3", 1));
    c->add(new Cube("perna4", 1));

    c->get(0)->transform(transform::scale(Vertex(1,0.1,2), Vertex(0,0,0)));

    c->get(1)->transform(transform::scale(Vertex(0.1,1,0.1), Vertex(0,0,0)));
    c->get(1)->transform(transform::translate(Vertex(0.45, -0.55, 0.95)));

    c->get(2)->transform(transform::scale(Vertex(0.1,1,0.1), Vertex(0,0,0)));
    c->get(2)->transform(transform::translate(Vertex(-0.45, -0.55, 0.95)));

    c->get(3)->transform(transform::scale(Vertex(0.1,1,0.1), Vertex(0,0,0)));
    c->get(3)->transform(transform::translate(Vertex(-0.45, -0.55, -0.95)));

    c->get(4)->transform(transform::scale(Vertex(0.1,1,0.1), Vertex(0,0,0)));
    c->get(4)->transform(transform::translate(Vertex(0.45, -0.55, -0.95)));

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++){
        ss->newObject((Cube*)c->get(i));
    }
}
void LEC::trash(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("lixeira");
    Cube* ob;

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("base", 1);
        ob->transform(transform::scale(Vertex(0.30,0.05,0.30), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,-0.25,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0,0,0),
                        new Vertex(0.2,0.2,0.2),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("lateral1", 1);
        ob->transform(transform::scale(Vertex(0.30,0.5,0.05), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0.025,0.125)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.3137,0.1764,0.0862),
                        new Vertex(0.2137,0.0764,0.0162),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("lateral2", 1);
        ob->transform(transform::scale(Vertex(0.30,0.5,0.05), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0.025,-0.125)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.3137,0.1764,0.0862),
                        new Vertex(0.2137,0.0764,0.0162),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("lateral3", 1);
        ob->transform(transform::scale(Vertex(0.05,0.5,0.2), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0.125,0.025,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.3137,0.1764,0.0862),
                        new Vertex(0.2137,0.0764,0.0162),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("lateral4", 1);
        ob->transform(transform::scale(Vertex(0.05,0.5,0.2), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(-0.125,0.025,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.3137,0.1764,0.0862),
                        new Vertex(0.2137,0.0764,0.0162),
                        1,0,1);
        c->add(ob);
    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::armario(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("armario");
    Cube* ob;

    // ----------------------------------------------------------------------- //
        // rigid-body transform
        ob = new Cube("corpo", 1);
        ob->transform(transform::scale(Vertex(1.6,1.6,0.6), Vertex(0,0,0)));
        //ob->transform(transform::translate(Vertex(0,0,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.3637,0.2264,0.1362),
                        new Vertex(0.3137,0.1764,0.1162),
                        1,0.03,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("divisão", 1);
        ob->transform(transform::scale(Vertex(0.01,1.6,0.01), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0,0.3)));
        // properties
        ob->setMaterial(new Vertex(0.1,0.1,0.1),
                        new Vertex(0.1,0.1,0.1),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("maçaneta1", 1);
        ob->transform(transform::scale(Vertex(0.05,0.05,0.05), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0.15,0,0.3)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.2137,0.0764,0.0162),
                        new Vertex(0.2137,0.0764,0.0162),
                        1,0.5,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("maçaneta2", 1);
        ob->transform(transform::scale(Vertex(0.05,0.05,0.05), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(-0.15,0,0.3)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.2137,0.0764,0.0162),
                        new Vertex(0.2137,0.0764,0.0162),
                        1,0.5,1);
        c->add(ob);
    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::PC(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("Computador");
    Cube* ob;

    // ----------------------------------------------------------------------- //
        // rigid-body transform
        ob = new Cube("monitor", 1);
        ob->transform(transform::scale(Vertex(0.5,0.4,0.05), Vertex(0,0,0)));
        //ob->transform(transform::translate(Vertex(0,0,0)));
        // properties
        ob->setMaterial(new Vertex(0.1,0.1,0.1),
                        new Vertex(0.1,0.1,0.1),
                        new Vertex(0,0,0),
                        1,0.05,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("tela", 1);
        ob->transform(transform::scale(Vertex(0.4,0.3,0.05), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0,0.01)));
        // properties
        ob->setMaterial(new Vertex(0.1,0.1,0.1),
                        new Vertex(0.3,0.3,0.3),
                        new Vertex(0,0,0),
                        1,0.1,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("mouse", 1);
        ob->transform(transform::scale(Vertex(0.05,0.03,0.1), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0.3,-0.195,0.15)));
        // properties
        ob->setMaterial(new Vertex(0.1,0.1,0.1),
                        new Vertex(0.3,0.3,0.3),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("teclado", 1);
        ob->transform(transform::scale(Vertex(0.45,0.01,0.15), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,-0.195,0.2)));
        // properties
        ob->setMaterial(new Vertex(0.1,0.1,0.1),
                        new Vertex(0.1,0.1,0.1),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("gabinete", 1);
        ob->transform(transform::scale(Vertex(0.17,0.4,0.35), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0.35,0,-0.1)));
        // properties
        ob->setMaterial(new Vertex(0.1,0.1,0.1),
                        new Vertex(0.1,0.1,0.1),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);
    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::profTable(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("Mesa Professor");
    Cube* ob;

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("tampo", 1);
        ob->transform(transform::scale(Vertex(0.75,0.04,0.6), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0.38,0)));
        // properties
        ob->setMaterial(new Vertex(0.1,0.1,0.1),
                        new Vertex(1,1,1),
                        new Vertex(1,1,1),
                        1,0.1,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("perna1", 1);
        ob->transform(transform::scale(Vertex(0.06,0.76,0.6), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0.345,-0.02,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.2137,0.0764,0.0162),
                        new Vertex(0.2137,0.0764,0.0162),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("perna2", 1);
        ob->transform(transform::scale(Vertex(0.06,0.76,0.6), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(-0.345,-0.02,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(0.2137,0.0764,0.0162),
                        new Vertex(0.2137,0.0764,0.0162),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::bigTable(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("Mesa Grande");
    Cube* ob;

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("tampo", 1);
        ob->transform(transform::scale(Vertex(2.8,0.04,0.6), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0.38,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(1,1,1),
                        new Vertex(1,1,1),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("perna central", 1);
        ob->transform(transform::scale(Vertex(0.06,0.76,0.6), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,-0.02,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(1,1,1),
                        new Vertex(1,1,1),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("perna1", 1);
        ob->transform(transform::scale(Vertex(0.06,0.76,0.6), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(-1.37,-0.02,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(1,1,1),
                        new Vertex(1,1,1),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("perna1", 1);
        ob->transform(transform::scale(Vertex(0.06,0.76,0.6), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(1.37,-0.02,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(1,1,1),
                        new Vertex(1,1,1),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("perna1", 1);
        ob->transform(transform::scale(Vertex(0.06,0.76,0.6), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0.67,-0.02,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(1,1,1),
                        new Vertex(1,1,1),
                        1,0,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("perna1", 1);
        ob->transform(transform::scale(Vertex(0.06,0.76,0.6), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(-0.67,-0.02,0)));
        // properties
        ob->setMaterial(new Vertex(0.2,0.2,0.2),
                        new Vertex(1,1,1),
                        new Vertex(1,1,1),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::projector(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("projetor");
    Cube* ob;

    // ----------------------------------------------------------------------- //
        // rigid-body transform
        ob = new Cube("cabeçote", 1);
        ob->transform(transform::scale(Vertex(0.3,0.15,0.3), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,-0.075,0)));
        // properties
        ob->setMaterial(new Vertex(0,0,0.2),
                        new Vertex(0,0,0.5),
                        new Vertex(0,0,0.5),
                        1,0.1,1);
        c->add(ob);

        // rigid-body transform
        ob = new Cube("suporte", 1);
        ob->transform(transform::scale(Vertex(0.1,0.15,0.1), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0.075,0)));
        // properties
        ob->setMaterial(new Vertex(0,0,0.2),
                        new Vertex(0,0,0.5),
                        new Vertex(0,0,0.5),
                        1,0.1,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::projectorBoard(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("projetor");
    Cube* ob;

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("quadro", 1);
        ob->transform(transform::scale(Vertex(1.5,2,0.01), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0,0)));
        // properties
        ob->setMaterial(new Vertex(0.1,0.1,0.1),
                        new Vertex(1,1,1),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::board(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("quadro");
    Cube* ob;

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("quadro", 1);
        ob->transform(transform::scale(Vertex(6.5,1,0.01), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0,0)));
        // properties
        ob->setMaterial(new Vertex(0,0.1,0.1),
                        new Vertex(0,0.4,0.4),
                        new Vertex(0,0,0),
                        1,0.4,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::door(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("porta");
    Cube* ob;

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("porta", 1);
        ob->transform(transform::scale(Vertex(0.7,2,0.03), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0,0)));
        // properties
        ob->setMaterial(new Vertex(0,0,0),
                        new Vertex(1,1,1),
                        new Vertex(0,0,0),
                        1,0,0);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::leftWall(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("parede");
    Cube* ob;

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("parede", 1);
        ob->transform(transform::scale(Vertex(12,3,0.1), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0,0)));
        // properties
        ob->setMaterial(new Vertex(0,0,0),
                        new Vertex(1,1,1),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::rightWallOverDoor(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("parede");
    Cube* ob;

    // ----------------------------------------------------------------------- //

        // rigid-body transform
        ob = new Cube("parede", 1);
        ob->transform(transform::scale(Vertex(0.7,1,0.1), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0,0)));
        // properties
        ob->setMaterial(new Vertex(0,0,0),
                        new Vertex(1,1,1),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));

}
void LEC::rightWall(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("parede");
    Cube* ob;

    // ----------------------------------------------------------------------- //
        // rigid-body transform
        ob = new Cube("parede", 1);
        ob->transform(transform::scale(Vertex(11.3, 3, 0.1), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0,0)));
        // properties
        ob->setMaterial(new Vertex(0,0,0),
                        new Vertex(1,1,1),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::backfrontWall(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("parede");
    Cube* ob;

    // ----------------------------------------------------------------------- //
        // rigid-body transform
        ob = new Cube("parede", 1);
        ob->transform(transform::scale(Vertex(6.6, 3, 0.1), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0,0)));
        // properties
        ob->setMaterial(new Vertex(0,0,0),
                        new Vertex(1,1,1),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::ceilFloor(Scene** s){
    Scene* ss = *s;
    Shape* c = new Shape_n("parede");
    Cube* ob;

    // ----------------------------------------------------------------------- //
        // rigid-body transform
        ob = new Cube("parede", 1);
        ob->transform(transform::scale(Vertex(6.6, 0.1, 12), Vertex(0,0,0)));
        ob->transform(transform::translate(Vertex(0,0,0)));
        // properties
        ob->setMaterial(new Vertex(0,0,0),
                        new Vertex(1,1,1),
                        new Vertex(0,0,0),
                        1,0,1);
        c->add(ob);

    // ----------------------------------------------------------------------- //

    ss->attach(c);
    int size = c->size();

    for(int i = 0 ; i < size ; i++) ss->newObject((Cube*)c->get(i));
}
void LEC::lineTable(Scene** s){
    Scene* ss = new Scene();
    Scene* output = *s;

    bigTable(&ss);

    chair(&ss);
    ss->shlist().at(1)->transform(transform::rotate(180, Vertex(0,1,0)));
    ss->shlist().at(1)->transform(transform::translate(Vertex(-1.05,0.1,0.5))); // z = 7.5

    chair(&ss);
    ss->shlist().at(2)->transform(transform::rotate(180, Vertex(0,1,0)));
    ss->shlist().at(2)->transform(transform::translate(Vertex(-0.35,0.1,0.5))); // z = 7.5

    chair(&ss);
    ss->shlist().at(3)->transform(transform::rotate(180, Vertex(0,1,0)));
    ss->shlist().at(3)->transform(transform::translate(Vertex(0.35,0.1,0.5))); // z = 7.5

    chair(&ss);
    ss->shlist().at(4)->transform(transform::rotate(180, Vertex(0,1,0)));
    ss->shlist().at(4)->transform(transform::translate(Vertex(1.05,0.1,0.5))); // z = 7.5

    // cadeira = (0.4,1,0.4)
    // mesa grande = (2.8, 0.8, 0.6)
    Shape* group = new Shape_n("agrupamento");

    for(unsigned int i = 0 ; i < ss->shlist().size() ; i++)
        group->add(ss->shlist().at(i));
    output->attach(group);
    for(unsigned int i = 0 ; i < ss->oblist().size() ; i++)
        output->newObject(ss->oblist().at(i));
}
void LEC::lineTablePC(Scene** s){
    Scene* ss = new Scene();
    Scene* output = *s;

    lineTable(&ss);
    PC(&ss);
    ss->shlist().at(1)->transform(transform::translate(Vertex(-1.15,0.60,0)));

    PC(&ss);
    ss->shlist().at(2)->transform(transform::translate(Vertex(-0.45,0.60,0)));

    PC(&ss);
    ss->shlist().at(3)->transform(transform::translate(Vertex(0.25,0.60,0)));

    PC(&ss);
    ss->shlist().at(4)->transform(transform::translate(Vertex(0.95,0.60,0)));

    // chao-teto = (6.6, 0.1, 12)
    // armario = (1.6,1.6,0.6)
    // lixeira = (0.3, 0.55, 0.3)
    // cadeira = (0.4, 1, 0.4)
    // mesa grande = (2.8, 0.8, 0.6)
    // mesa-cadeiras = (2.8, 1, 1)
    //

    Shape* group = new Shape_n("agrupamento");
    for(unsigned int i = 0 ; i < ss->shlist().size() ; i++)
        group->add(ss->shlist().at(i));
    output->attach(group);
    for(unsigned int i = 0 ; i < ss->oblist().size() ; i++)
        output->newObject(ss->oblist().at(i));
}
void LEC::profTablePC(Scene** s){
    Scene* ss = new Scene();
    Scene* output = *s;

    profTable(&ss);

    chair(&ss);
    ss->shlist().at(1)->transform(transform::rotate(180, Vertex(0,1,0)));
    ss->shlist().at(1)->transform(transform::translate(Vertex(0, 0.125, 0.5)));

    PC(&ss);
    ss->shlist().at(2)->transform(transform::translate(Vertex(-0.1, 0.6, 0.05)));

    Shape* group = new Shape_n("agrupamento");
    for(unsigned int i = 0 ; i < ss->shlist().size() ; i++)
        group->add(ss->shlist().at(i));
    output->attach(group);
    for(unsigned int i = 0 ; i < ss->oblist().size() ; i++)
        output->newObject(ss->oblist().at(i));
}
void LEC::buildLEC(Scene** s){
    Scene* ss = *s;
    ceilFloor(s);

    armario(s);
    ss->shlist().at(1)->transform(transform::rotate(180, Vertex(0,1,0)));
    ss->shlist().at(1)->transform(transform::translate(Vertex(-2.5,0.85,5.7))); // z = 7.5

    trash(s);
    ss->shlist().at(2)->transform(transform::translate(Vertex(-1.55,0.325,5.85)));

    lineTable(s);
    ss->shlist().at(3)->transform(transform::translate(Vertex(-1.9,0.45,4.4)));

    lineTable(s);
    ss->shlist().at(4)->transform(transform::translate(Vertex(1.9,0.45,4.4)));

    lineTablePC(s);
    ss->shlist().at(5)->transform(transform::translate(Vertex(-1.9,0.45,2.4)));

    lineTablePC(s);
    ss->shlist().at(6)->transform(transform::translate(Vertex(1.9,0.45,2.4)));

    lineTablePC(s);
    ss->shlist().at(7)->transform(transform::translate(Vertex(-1.9,0.45,0.4)));

    lineTablePC(s);
    ss->shlist().at(8)->transform(transform::translate(Vertex(1.9,0.45,0.4)));

    lineTablePC(s);
    ss->shlist().at(9)->transform(transform::translate(Vertex(-1.9,0.45,-1.6)));

    lineTablePC(s);
    ss->shlist().at(10)->transform(transform::translate(Vertex(1.9,0.45,-1.6)));

    profTablePC(s);
    ss->shlist().at(11)->transform(transform::rotate(-90, Vertex(0,1,0)));
    ss->shlist().at(11)->transform(transform::translate(Vertex(3,0.45,-3.5)));

    backfrontWall(s);
    ss->shlist().at(12)->transform(transform::scale(Vertex(1,1.033333333,1), Vertex(0,0,0)));
    ss->shlist().at(12)->transform(transform::translate(Vertex(0,1.55,-6.05)));

    backfrontWall(s);
    ss->shlist().at(13)->transform(transform::scale(Vertex(1,1.033333333,1), Vertex(0,0,0)));
    ss->shlist().at(13)->transform(transform::translate(Vertex(0,1.55,6.05)));

    door(s);
    ss->shlist().at(14)->transform(transform::rotate(-90, Vertex(0,1,0)));
    ss->shlist().at(14)->transform(transform::translate(Vertex(3.285,1.05,5.65)));

    leftWall(s);
    ss->shlist().at(15)->transform(transform::rotate(-90, Vertex(0,1,0)));
    ss->shlist().at(15)->transform(transform::translate(Vertex(-3.35,1.55,0)));

    rightWall(s);
    ss->shlist().at(16)->transform(transform::rotate(-90, Vertex(0,1,0)));
    ss->shlist().at(16)->transform(transform::translate(Vertex(3.3,1.55,-0.35)));

    rightWallOverDoor(s);
    ss->shlist().at(17)->transform(transform::rotate(-90, Vertex(0,1,0)));
    ss->shlist().at(17)->transform(transform::translate(Vertex(3.25,2.5,5.625)));

    projector(s);
    ss->shlist().at(18)->transform(transform::translate(Vertex(0,2.8,-1)));

    projectorBoard(s);
    ss->shlist().at(19)->transform(transform::translate(Vertex(0,1.5,-5.985)));

    board(s);
    ss->shlist().at(20)->transform(transform::translate(Vertex(0,1.5,-5.995)));

    ceilFloor(s);
    ss->shlist().at(21)->transform(transform::translate(Vertex(0,3,0)));

    // chao-teto = (6.6, 0.1, 12)
    // armario = (1.6,1.6,0.6)
    // lixeira = (0.3, 0.55, 0.3)
    // cadeira = (0.4, 1, 0.4)
    // mesa grande = (2.8, 0.8, 0.6)
    // mesa-cadeiras = (2.8, 1, 1)
    // parede sobre a porta = (0.7,1,0.1)
    // parede post-ant = (6.6, 3, 0.1)

}
