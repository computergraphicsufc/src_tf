#include "include/components/rendering.h"
#include "include/primitives/cube.h"
#include "include/components/object_component.h"
#include "include/components/vertex_component.h"
#include "include/components/matrix_component.h"

#include <GL/glut.h>

#define MAXIMO 2
#define BACKGROUND Vertex(0,0,0)
#define STEP 1

Scene* scene;
MatrixColor* mc;
MainWindow* window;

// -------------------------------------------------------------------------------------------------------------------- //

// Render Functions
void render::shading(Scene* s){
    int objects = s->oblist().size();
    Face *f;
    Object* o;
    int sizef;
    Vertex *v1, *v2, *v3;
    Material* m;

    for(int i = 0 ; i < objects ; i++){
        o = s->oblist().at(i);
        sizef = o->fSize();
        m = o->getMaterial();

        glMaterialfv(GL_FRONT, GL_AMBIENT, m->getAmbient()->get());
        glMaterialfv(GL_FRONT, GL_DIFFUSE, m->getDifuse()->get());
        glMaterialfv(GL_FRONT, GL_SPECULAR, m->getSpecular()->get());
        glMaterialf(GL_FRONT, GL_SHININESS, m->getShininess());

        for(int index = 0 ; index < sizef ; index++){
            f = o->fat(index);
            v1 = f->get(0); v2 = f->get(1); v3 = f->get(2);
            glBegin(GL_TRIANGLES);
                glVertex4fv(v1->get());
                glVertex4fv(v2->get());
                glVertex4fv(v3->get());
            glEnd();
        }// for
    }
}// render

// -------------------------------------------------------------------------------------------------------------------- //

Vertex* render::getColor(Vertex v, Vertex l, Vertex n, Object* o, Light* light, Vertex pint){
    float m = o->getMaterial()->getShininess();
    float ln = vertex_fun::dot(n,l);

    if(ln < 0) ln = 0;

    Vertex r = vertex_fun::minus((vertex_fun::mult(2*ln, n)), l);
    float rvm = pow(vertex_fun::dot(r,v), m);    

    if(rvm < 0) rvm = 0;

    Vertex* md = o->getMaterial()->getDifuse();
    Vertex Id = vertex_fun::mult(ln, (vertex_fun::mult(*(light->getDifuse()), *md)));
    float d = vertex_fun::length(vertex_fun::minus(*light->getPosition(), pint));
    Id = vertex_fun::mult(1/(1 + 0*d + 0*d*d), Id);

    Vertex* ms = o->getMaterial()->getSpecular();
    Vertex Is = vertex_fun::mult(rvm, (vertex_fun::mult(*(light->getSpecular()), *ms)));

    Vertex* ma = o->getMaterial()->getAmbient();
    Vertex Ia = vertex_fun::mult(*(light->getAmbient()), *ma);
    Vertex It = vertex_fun::sum((vertex_fun::sum(Id, Is)),Ia);

    if(It.get(0) > 1) It.set(1,0);
    if(It.get(1) > 1) It.set(1,1);
    if(It.get(2) > 1) It.set(1,2);

    return new Vertex(It.get(0),It.get(1),It.get(2));
}// getColor

// -------------------------------------------------------------------------------------------------------------------- //

float render::intersec(Vertex base, Vertex vetor, Face* f){

    Vertex ut = vertex_fun::minus(*(f->get(2)), *(f->get(0)));
    Vertex vt = vertex_fun::minus(*(f->get(1)), *(f->get(0)));
    Vertex n1 = vertex_fun::cross(vt,ut);

    float t = - (vertex_fun::dot(base, n1) - vertex_fun::dot(n1, *(f->get(0))) )/(vertex_fun::dot(vetor, n1));
    Vertex P = vertex_fun::sum(base, vertex_fun::mult(t, vetor));

    Vertex* t1, *t2;
    float d;

    for(int index = 0 ; index < 3 ; index++ ){
        t1 = f->get(index);
        t2 = f->get((index+1)%3);

        Vertex v1 = vertex_fun::minus(*t1, P);
        Vertex v2 = vertex_fun::minus(*t2, P);

        Vertex n2 = vertex_fun::norm(vertex_fun::cross(v2,v1));
        d = - (vertex_fun::dot(base, n2));

        if(vertex_fun::dot(P, n2) + d < 0.000001){
            return -1;
        }// if
    }// for
    return t;
}// intersect

// -------------------------------------------------------------------------------------------------------------------- //

/**
  sceneIntersect: procura uma intersecção entre vetor entre um ponto e o cenário.
*/

float render::sceneIntersect(Vertex base, Vertex vector, Scene* s, Face** fc, Object** oc){
    std::vector<Object*> objects = s->oblist();
    int size = objects.size();
    float min = INFINITY, a;
    Object* temp;
    Face* ft;

    for(int i = 0 ; i < size ; i++){
        temp = objects.at(i);

        if(sphereIntersection(base, vector, temp)){
            int fsize = temp->fSize();
            for(int j = 0 ; j < fsize ; j++){
                ft = temp->fat(j);
                a = intersec(base,vector,ft);
                if(a < min && a > 0.0001){
                    min = a;
                    *oc = temp;
                    *fc = ft;
                }
            }// for-2
        }// if
    }// for-1
    if(min < INFINITY) return min;
    return -1;
}// sceneIntersect

// -------------------------------------------------------------------------------------------------------------------- //

MatrixColor* render::raycasting(std::vector<Object*> group, Camera* cam, Light* light){

    // variables
    int sog = group.size();
    int sf;
    Object* o, *choose;
    Face *f, *fchosen;
    float a,x,y, min;
    Vertex *pixel;
    Screenshot* screen = cam->getScreenshot();
    float d = screen->getDistance();

    bool intersection = false;

    float W = screen->getScreenWidth();
    float H = screen->getScreenHeight();
    int Nx = screen->getWindowWidth();
    int Ny = screen->getWindowHeight();


    MatrixColor* matrix = new MatrixColor(Ny, Nx);
    matrix->setAll(1,1,1);
    // variables

    // ------------------------------------------------------

    for(int i1 = 0 ; i1 < Ny ; i1++){
        x = render::x(W,Nx,i1);
        for(int i2 = 0 ; i2 < Nx ; i2++){
            y = render::y(H,Ny,i2);

            min = INFINITY;
            Vertex v = vertex_fun::norm(Vertex(x,y,-d));

                for(int i3 = 0 ; i3 < sog ; i3++){
                    o = group.at(i3);
                    sf = o->fSize();

                    //if(sphereIntersection(Vertex(x,y,0),Vertex(x,y,-d),o)){
                        for(int i4 = 0 ; i4 < sf ; i4++){
                            f = o->fat(i4);

                            a = intersec(Vertex(x,y,-d), v, f);

                            if(a >= 0 && a < min){

                                min = a;
                                choose = o;
                                fchosen = f;
                                intersection = true;
                            }// if
                        }// for - 4
//                    }// interseção com esfera
                }// for-3

                if(intersection){
                    Vertex Pint = vertex_fun::sum(Vertex(x,y,0), vertex_fun::mult(min, v));
                    Vertex l = vertex_fun::norm(vertex_fun::minus(*(light->getPosition()), Pint));
                    Vertex viewer = vertex_fun::norm(vertex_fun::minus(Vertex(0,0,0), Pint)); // ALTERAR DEPOIS PRA PLANO
                    Vertex normal = vertex_fun::norm(vertex_fun::cross(vertex_fun::minus(*(fchosen->get(1)), *(fchosen->get(0))),
                                                                       vertex_fun::minus(*(fchosen->get(2)), *(fchosen->get(0)))));
                    pixel = render::getColor(viewer, l, normal, choose, light, Pint);
                    matrix->set(pixel, i1, i2);
                }// if
            intersection = false;

        }//for-2
    }//for-1

    return matrix;
}// raycasting

// -------------------------------------------------------------------------------------------------------------------- //

bool render::shadow(Vertex point, Vertex vetor ,std::vector<Object*> olist){
    Object* o;
    Face* f;
    int size = olist.size();
    int sf;

    for(int i = 0 ; i < size ; i++){
        o = olist.at(i);
        sf = o->fSize();

        for(int j = 0 ; j < sf ; j++){
            f = o->fat(j);
            float a = intersec(point, vetor, f);

            if(a > 0.000001)
                return true;
        }// for-2
    }// for-1
    return false;
}

// -------------------------------------------------------------------------------------------------------------------- //

bool render::sphereIntersection(Vertex Point, Vertex vetor, Object* o){
    float radius = 0;
    Vertex center(0,0,0);
    radius = o->getSphereBound();
    center = o->getSphereCenter();

    float b = 2*vertex_fun::dot(vetor, vertex_fun::minus(Point, center));
    float temp = vertex_fun::length(vertex_fun::minus(Point, center));
    float c = temp*temp - radius*radius;

    float discriminant = b*b - 4*c;
    if(discriminant < 0)
        return false;
    return true;
}

// -------------------------------------------------------------------------------------------------------------------- //
/*
bool render::areaIntersection(Vertex p1, Vertex p4, std::vector<Object*> olist){
    Vertex x(0,0,0);
    float a = 0;
    float d;
    int size = olist.size();
    Object* o;
    Vertex array[4] = {p1, Vertex(p4.get(0),p1.get(1),p1.get(2)), Vertex(p1.get(0),p4.get(1),p1.get(2)), p4};

    if(p1.get(0) <= x.get(0) && p1.get(1) >= x.get(1) &&
       p4.get(0) >= x.get(0) && p4.get(1) <= x.get(1)) return true;

    for(int i = 0 ; i < size ; i++){
        o = olist.at(i);
        a = object::getSphereBound(o);
        x = object::getSphereCenter(o);
        x.set(0,2);

        for(int j = 0 ; j < 4 ; j++){
            d = vertex_fun::length(vertex_fun::minus(array[j],x));
            if(d <= a)
                return true;
        }// for-2
    }// for-1
    return false;
}// areaIntersection
*/
// -------------------------------------------------------------------------------------------------------------------- //

Matrix projection::perspective(float xmin, float xmax, float ymin, float ymax, float near, float far){
    float shx = -((xmin+xmax)/(2*far)); // fator de cisalhamento na direção x
    float shy = -((ymin+ymax)/(2*far)); // fator de cisalhamento na direção y

    Matrix ret = matrix::identity();

    Matrix C(1,0,shx,0,
             0,1,shy,0,
             0,0,1,0,
             0,0,0,1);

    ret = matrix_fun::mult(C,ret);

    float sx = (2*near)/(xmax - xmin); // fator de escala na direção x
    float sy = (2*near)/(ymax - ymin); // fator de escala na direção y

    Matrix S(sx,0,0,0,
             0,sy,0,0,
             0,0,1,0,
             0,0,0,1);

    ret = matrix_fun::mult(S,ret);

    float zz = (far+near)/(far-near);
    float zzz = (2*far*near)/(far-near);

    Matrix N(1,0,0,0,
             0,1,0,0,
             0,0,zz,zzz,
             0,0,-1,0);
    ret = matrix_fun::mult(N, ret);
    return ret;
}

// -------------------------------------------------------------------------------------------------------------------- //

MatrixColor* render::raytracing(Scene* s){

    window->setProgress(0);

    // variables
    float x,y;
    Screenshot* screen = s->getCamera()->getScreenshot();
    float d = screen->getDistance();

    float W = screen->getScreenWidth();
    float H = screen->getScreenHeight();
    int Nx = screen->getWindowWidth();
    int Ny = screen->getWindowHeight();


    MatrixColor* matrix = new MatrixColor(Ny, Nx);
    matrix->setAll(1,1,1);
    // variables

    // ------------------------------------------------------

    for(int i1 = 0 ; i1 < Ny ; i1++){
        x = render::x(W,Nx,i1);
        for(int i2 = 0 ; i2 < Nx ; i2++){
            y = render::y(H,Ny,i2);
            Vertex v = vertex_fun::norm(Vertex(x,y,-d));

            Vertex pixel = trace(Vertex(x,y,0), v, 0, s);

            matrix->set(new Vertex(pixel.get(0),pixel.get(1),pixel.get(2)), i1, i2);
            float a = Nx*Ny;
            float b = i1*Nx + i2;

            window->setProgress((b/a)*100);
        }//for-2
    }//for-1

    window->setProgress(100);
    return matrix;
}

// -------------------------------------------------------------------------------------------------------------------- //

Vertex render::trace(Vertex point,Vertex vector,float step, Scene* s){
    if(step >= MAXIMO) return BACKGROUND;

    Object* o;
    Face* f;

    // intersection
    float intersection = sceneIntersect(point, vector, s, &f, &o);

    if(intersection < 0.000001){ return BACKGROUND;}
    // intersection

    Vertex pint = vertex_fun::sum(point, vertex_fun::mult(intersection, vector));
    Vertex normal = vertex_fun::norm(vertex_fun::cross(vertex_fun::minus(*f->get(1),*f->get(0)),
                                                       vertex_fun::minus(*f->get(2),*f->get(0))));

    // reflection vector
    Vertex refl = vertex_fun::norm(vertex_fun::minus(vector, pint));
    refl = vertex_fun::minus(vertex_fun::mult(2*vertex_fun::dot(refl, normal), normal), refl);
    refl = vertex_fun::norm(refl);
    // reflection vector

    Vertex reflection = trace(pint, refl, step + STEP, s);
    reflection = vertex_fun::mult(o->getMaterial()->getReflectivity(), reflection);

    Vertex ll = vertex_fun::norm(vertex_fun::minus(point, pint));
    Vertex transmission = difraction(ll, normal,
                                     o->getMaterial()->getDifraction());
    transmission = trace(pint, transmission, step + STEP, s);
    transmission = vertex_fun::mult(1 - o->getMaterial()->getTransparency(), transmission);

    // local calculations
    Vertex l = vertex_fun::minus(*s->getLight()->getPosition(), pint);
    l = vertex_fun::norm(l);

    Vertex v = vertex_fun::inverse(pint);
    v = vertex_fun::norm(v);

    Vertex* x = getColor(v, l, normal, o, s->getLight(), pint);
    Vertex local = *x;
    //local = vertex_fun::mult(o->getMaterial()->getReflectivity(), local);
    delete x;

    // local calculations

    Vertex output = vertex_fun::sum(reflection, vertex_fun::sum(transmission, local));
    return output;
}// trace

// -------------------------------------------------------------------------------------------------------------------- //
// OTHERS

float render::x(float W, int Nx, int i1){
    return - (W/2) + (W/Nx)/2 + i1*(W/Nx);
}//x

// -------------------------------------------------------------------------------------------------------------------- //

float render::y(float H, int Ny, int i2){
    return H/2 - (H/Ny)/2 - i2*(H/Ny);
}//y

// OTHERS

// -------------------------------------------------------------------------------------------------------------------- //

void render::display(){
    //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    Camera* camera = scene->getCamera();
    Screenshot* xote = camera->getScreenshot();

    float W = xote->getScreenWidth();
    float H = xote->getScreenHeight();
    int Nx = xote->getWindowWidth();
    int Ny = xote->getWindowHeight();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-W/2,W/2,-H/2,H/2,-1,1);
    glMatrixMode(GL_MODELVIEW);

    float x, y;
    for(int i = 0 ; i < Ny ; i++){
        for(int j = 0 ; j < Nx ; j++){
            x = - (W/2) + (W/Nx)/2 + i*(W/Nx);
            y = H/2 - (H/Ny)/2 - j*(H/Ny);

            glPointSize(W/Nx);
            glBegin(GL_POINTS);
                glColor3fv(mc->get(i,j)->get());
                glVertex3f(x,y,0);
            glEnd();
        }//for -2
    }// for-1

    //glutSwapBuffers();
}// display

void render::debug_draw(Shape* s){
    Vertex a = s->getSphereCenter();

    if(s->type() == SHAPE_N){
        glColor3f(1,1,0);
    } else {
        glColor3f(0,1,1);
    }

    a = Vertex(a.get(0), a.get(1), 0);
    std::cout << a << std::endl;

    glBegin(GL_POINTS);
        glVertex3f(0.5,0.5,0);
        glVertex3fv(a.get());
    glEnd();

    if(s->type() == SHAPE_N){
        int ab = s->size();
        for(int i = 0 ; i < ab ; i++)
            debug_draw(s->get(i));
    }// if
}// debug_draw

// ---------------------------------------------------------------------------------------------------------------- //

void render::send(Scene* s){
    if(!scene) delete scene;
    scene = s;
}

Scene* render::get(){
    return scene;
}

void render::updateCamera(){
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-5,5,-5,5,1,50);
    //glOrtho(-5,5,-5,5,-5, 5);

    Vertex* eye = scene->getCamera()->getPosition();
    Vertex* lookat = scene->getCamera()->getLookAt();
    Vertex* vup = scene->getCamera()->getVup();

    gluLookAt(eye->get(0), eye->get(1), eye->get(2),
              lookat->get(0), lookat->get(1), lookat->get(2),
              vup->get(0), vup->get(1), vup->get(2));
    glMatrixMode(GL_MODELVIEW);

}

Vertex render::difraction(Vertex trans, Vertex normal, float dif){
    Vertex t1 = vertex_fun::mult(-dif, trans);
    float t2 = vertex_fun::dot(normal, trans);
    float t4 = dif*t2;
    float t3 = sqrt(1 - (dif*dif)*(1 - t2*t2));
    Vertex t5 = vertex_fun::mult(t4 - t3, normal);
    Vertex output = vertex_fun::sum(t1,t5);
    output = vertex_fun::norm(output);

    return output;
}

void render::constructImage(){
    mc =raytracing(scene);
}

bool intersection = false;

bool render::groupIntersection(Vertex Point,Vertex vetor, Shape* s){
    float radius = 0;
    Vertex center(0,0,0);
    radius = s->getSphereBound();
    center = s->getSphereCenter();

    float b = 2*vertex_fun::dot(vetor, vertex_fun::minus(Point, center));
    float temp = vertex_fun::length(vertex_fun::minus(Point, center));
    float c = temp*temp - radius*radius;

    float discriminant = b*b - 4*c;
    if(discriminant < 0)
        return false;
    return true;
}

float render::treeIntersection(Vertex base, Vertex vector, Scene* s, Face** fc, Object** oc){
    float min = INFINITY, a;
    Object* temp;
    Face* ft;

    intersection = false;
    Shape* group;
    int size = s->getrglist().size();

    for(int i = 0 ; i < size ; i++){
        group = s->getrglist().at(i);
        if(searchIntersection(base, vector, group, &temp)){
            int fsize = temp->fSize();
            for(int j = 0 ; j < fsize ; j++){
                ft = temp->fat(j);
                a = intersec(base,vector,ft);
                if(a < min && a > 0.0001){
                    min = a;
                    *oc = temp;
                    *fc = ft;
                }
            }// for-2
        }// if
    }// for

    if(min < INFINITY) return min;
    return -1;
}

bool render::searchIntersection(Vertex base, Vertex vetor, Shape* node, Object** output){
    if(!groupIntersection(base, vetor , node)) return false;

    int size = node->size();
    bool b;
    for(int i = 0 ; i < size ; i++)
        searchIntersection(base, vetor, node->get(i), output);

    if(node->type() > 1 && !intersection){
        intersection = true;
        *output = (Object*)node;
    }
    return true;
}// searchIntersection

void render::setWidget(MainWindow* w){
    window = w;
}

MainWindow* render::getWidget(){
    return window;
}
