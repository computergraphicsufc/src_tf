#-------------------------------------------------
#
# Project created by QtCreator 2017-01-11T21:40:18
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = v-05
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    internal/components/create_component.cpp \
    internal/components/matrix_component.cpp \
    internal/components/object_component.cpp \
    internal/components/predefined.cpp \
    internal/components/rendering.cpp \
    internal/components/transform_component.cpp \
    internal/components/vertex_component.cpp \
    internal/illumination/light.cpp \
    internal/input-output/reader.cpp \
    internal/input-output/writer.cpp \
    internal/model/face.cpp \
    internal/model/material.cpp \
    internal/model/matrix.cpp \
    internal/model/matrixcolor.cpp \
    internal/model/object.cpp \
    internal/model/scene.cpp \
    internal/model/shape.cpp \
    internal/model/shape_n.cpp \
    internal/model/vertex.cpp \
    internal/observer/camera.cpp \
    internal/observer/screenshot.cpp \
    internal/primitives/cube.cpp \
    internal/primitives/piramid.cpp \
    render.cpp \
    glwidget.cpp

HEADERS  += mainwindow.h \
    include/components/create_component.h \
    include/components/matrix_component.h \
    include/components/object_component.h \
    include/components/predefined.h \
    include/components/rendering.h \
    include/components/transform_component.h \
    include/components/vertex_component.h \
    include/illumination/light.h \
    include/input-output/reader.h \
    include/input-output/writer.h \
    include/model/entity.h \
    include/model/face.h \
    include/model/material.h \
    include/model/matrix.h \
    include/model/matrixcolor.h \
    include/model/object.h \
    include/model/scene.h \
    include/model/shape.h \
    include/model/shape_n.h \
    include/model/spherebound.h \
    include/model/vertex.h \
    include/observer/camera.h \
    include/observer/screenshot.h \
    include/primitives/cube.h \
    include/primitives/piramid.h \
    render.h \
    glwidget.h

FORMS    += mainwindow.ui \
    render.ui

LIBS += -lglut -lGL -lGLU
