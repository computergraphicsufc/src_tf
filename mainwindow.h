#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setProgress(float);

private:
    Ui::MainWindow *ui;

public slots:
    void up();
    void down();
    void left();
    void right();

    void render();
    void camera();

    void rw();
    void lw();
    void dw();
    void uw();
};

#endif // MAINWINDOW_H
