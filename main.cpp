#include "mainwindow.h"
#include "render.h"
#include <QApplication>

#include <GL/glut.h>
#include "include/components/rendering.h"

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    QApplication a(argc, argv);

    MainWindow w;
    render::setWidget(&w);
    w.show();

    return a.exec();
}
