#include "render.h"
#include "ui_render.h"

Render::Render(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Render)
{
    ui->setupUi(this);
}

Render::~Render()
{
    delete ui;
}

void Render::raytrace(MatrixColor* m){
    QImage image = QImage(700, 700, QImage::Format_RGB32);

    Vertex* color;
    for(int index = 0 ; index < 700 ; index++){
        for(int i2 = 0 ; i2 < 700 ; i2++){
            color = m->get(index, i2);
            image.setPixel(index, i2, qRgb(color->get(0)*255,color->get(1)*255,color->get(2)*255));
        }// for-2
    }// for-1

    if(!ui->graphicsView->scene()) delete ui->graphicsView->scene();

    QGraphicsScene* graphic = new QGraphicsScene(this);
    graphic->addPixmap(QPixmap::fromImage(image));
    ui->graphicsView->setScene(graphic);

    delete m;
}
