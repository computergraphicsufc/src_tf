#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include "object.h"

#include "include/observer/camera.h"
#include "include/illumination/light.h"

// Camera Movement
#define LEFT 9
#define RIGHT 8
#define FRONT 10
#define BACK 11
#define G_LEFT 12
#define G_RIGHT 13
#define G_UP 14
#define G_DOWN 15
// Camera Movement

class Scene{
private:
    std::vector<Object*> olist;
    std::vector<Shape*> glist;
    std::vector<Shape*> renderglist;
    Camera* camera;
    Light* light;

    Shape* group;
    float distance = 1;
public:
    Scene();

    //scene functions
    Object* duplicateObject(int);
    Shape* duplicateShape(int);

    std::vector<Shape*> getrglist(){return renderglist;}
    //scene functions

    void setCamera(Camera*);
    void setLight(Light*);

    // frame conversion
    void worldcam();
    void camworld();
    // frame conversion

    // camera movement
    void walk(int);
    void girate(int);
    // camera movement

    // objects functions
    void newObject(Object*);
    void delObject(Object*);

    // groups functions
    void attach(Shape*);
    void detach(int);

    bool groupby(Object*, Shape*);
    Shape* getGroup();
    void constructTree();
    void constructNode(Object*, Shape*, Shape**, float*);

    void constructGroup();
    void constructGroupSearch(Shape*);

    // getters
    std::vector<Object*> oblist();
    std::vector<Shape*> shlist();
    Camera* getCamera();
    Light* getLight();
    // getters

    friend std::ostream& operator << (std::ostream& os, Scene s){
        int so = s.olist.size();

        os << "s\n";
            for(int i = 0 ; i < so ; i++)
                os << *s.olist.at(i);
            os << s.camera;
            os << *s.light;
        os << "?s\n";
        return os;
    }
};

#endif

