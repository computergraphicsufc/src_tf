#ifndef SPHEREBOUND_H
#define SPHEREBOUND_H

#include "vertex.h"

class SphereBound{
public:
    virtual float getSphereBound() = 0;
    virtual Vertex getSphereCenter() = 0;
};

#endif
