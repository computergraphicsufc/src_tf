#ifndef ENTITY_H
#define ENTITY_H

#include "matrix.h"

#define SIZE 700
#define END "?"

class Entity{
protected:
    bool ingroup;
public:
    virtual void transform(Matrix) = 0;

    void setInGroup(bool a){ingroup = a;}
    bool inGroup(){return ingroup;}
};

#endif
