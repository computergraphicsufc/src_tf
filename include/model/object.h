#ifndef OBJECT_H
#define OBJECT_H

#include <vector>

#include "shape.h"
#include "face.h"
#include "material.h"
#include "spherebound.h"
#include "entity.h"

class Object : public Shape
{
protected:
    std::vector<Face*> fList;
    std::vector<int*> fpointers;
    std::vector<Vertex*> vList;
    Material* material;
public:
    Object(std::string);
    ~Object();

    // class methods
    void addVertex(Vertex*);
    void addFace(int,int,int);
    void setMaterial(Vertex*,Vertex*,Vertex*,float,float,float);

    Material* getMaterial();
    int fSize();
    int vSize();
    Vertex* vat(int);
    Face* fat(int);

    Object* duplicate();
    // class methods

    // -------------------------- //

    // shape methods
    int type();
    std::string getName();
    void add(Shape *);
    Shape* get(std::string);
    Shape* get(int);
    int size();
    void print(int);
    // shape methods

    // spherebound methods
    float getSphereBound();
    Vertex getSphereCenter();
    // spherebound methods

    // entity method
    void transform(Matrix);
    // entity method
    // -------------------------- //
/*
    friend std::ostream& operator << (std::ostream& os, Object o){
        int sv = o.vSize();
        int sf = o.fpointers.size();

        os << "o\n";
        os << o.name << "\n";
        os << o.Type << "\n";
        os << *o.material;
        os << "lv\n";
        for(int index = 0 ; index < sv ; index++)
             os << *(o.vList.at(index));
        os << END << "lv\n";

        os << "lf\n";
        for(int i = 0 ; i < sf ; i++){
            os << "f\n";
            for(int j = 0 ; j < 3 ; j++){
                os << o.fpointers.at(i)[j] << "\n";
            }// for-2
            os << END << "f\n";
        }// for-1
        os << END << "lf\n";
        os << "?o\n";
        return os;
    }//
*/

    friend std::ostream& operator << (std::ostream& os, Object o){
       os << o.name << ":\n";
       os << "Vertex list:\n";
       int sv = o.vSize();
       int sf = o.fSize();
       int ss = o.fpointers.size();
       for(int index = 0 ; index < sv ; index++)
            os << *(o.vList.at(index)) << "\n";
       os << "Face list:\n";
       for(int index = 0 ; index < sf ; index++)
            os << *(o.fList.at(index)) << "\n";
       os << "integers\n";
       for(int i = 0 ; i < ss ; i++){
           os << "[ ";
           for(int j = 0 ; j < 3 ; j++){
               os << o.fpointers.at(i)[j] << " ";
           }
           os << "]\n";
       }
       return os;
    }// tostring
};


#endif // OBJECT_H
