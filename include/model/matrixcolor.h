#ifndef MATRIXCOLOR_H
#define MATRIXCOLOR_H

#include"vertex.h"

class MatrixColor
{
    Vertex*** matrix;
    int column, row;
public:
    MatrixColor(int,int);
    ~MatrixColor();

    Vertex* get(int, int);
    int columns();
    int rows();
    void set(Vertex*, int, int);
    void setAll(float, float, float);

    friend std::ostream& operator << (std::ostream& os, MatrixColor m){
        os << "Matrix:\n";
        for(int i = 0 ; i < m.row ; i++){
            for(int j = 0 ; j < m.column ; j++){
                os << *(m.matrix[i][j]) << " ";
            }// for - 2
            os << "\n";
        }// for - 1
        return os;
    }// tostring
};

#endif // MATRIXCOLOR_H
