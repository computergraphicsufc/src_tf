#ifndef OBJ_NODE_H
#define OBJ_NODE_H

#include "obj.h"
#include <vector>

class Obj_node : public Obj
{
private:
    std::vector<Obj*> group;
public:
    Obj_node();
    void add(Obj*);
    Obj* get(int);
    int size();
    void debug();
};

#endif // OBJ_NODE_H
