#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include "include/model/entity.h"
#include "include/model/spherebound.h"

#define SHAPE 0
#define SHAPE_N 1

#define OBJECT 2
#define CUBE 3
#define PIRAMID 4

class Shape : public Entity, public SphereBound
{
protected:
    int Type;
    std::string name;
public:
    Shape(std::string);

    virtual void add(Shape*) = 0;
    virtual Shape* get(std::string) = 0;
    virtual Shape* get(int) = 0;
    virtual int type() = 0;
    virtual std::string getName() = 0;
    virtual int size() = 0;

    void transform(Matrix);

    virtual void print(int) = 0;
};

#endif // SHAPE_H
