#ifndef VERTEX_H
#define VERTEX_H

#include <iostream>
#include "entity.h"

/**

  Classe Vertex:
    Representa: ponto, vértice, vetor e atributos como cor, reflectividade e intensidade luminosa


*/

class Vertex
{
private:
    float c[4];

public:
    Vertex(float, float, float);
    Vertex();
    //~Vertex();
    void set(float, int);
    float get(int);
    float* get();

    Vertex* duplicate();
/*
    friend std::ostream& operator << (std::ostream& os, Vertex u){
        os << "v\n";
        os << u.c[0] << "\n";
        os << u.c[1] << "\n";
        os << u.c[2] << "\n";
        os << END << "v\n";
        return os;
    }
*/

    friend std::ostream& operator <<(std::ostream& os, Vertex u){
        return os << "[" << u.c[0] << ", " <<
              u.c[1] << ", " << u.c[2] << ", " << u.c[3] <<
              "]";
    }// toString

};

#endif // VERTEX_H
