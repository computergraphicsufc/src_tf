#ifndef FACE_H
#define FACE_H

#include"vertex.h"

class Face
{
private:
    Vertex* f[3];
public:
    Face(Vertex*, Vertex*, Vertex*);
    void set(Vertex*, int);
    Vertex* get(int);
    ~Face();

    friend std::ostream& operator <<(std::ostream& os, Face fa){
        os << "f\n";
        os << *fa.f[0];
        os << *fa.f[1];
        os << *fa.f[2];
        os << END << "f\n";
        return os;
    }

/*  DEBUG
    friend std::ostream& operator <<(std::ostream& os, Face fa){
        return os << "face:\n" << *(fa.f[0]) << "\n" <<
                  *(fa.f[1]) << "\n" << *(fa.f[2]) << "\n";
    }// <<
*/
};

#endif // FACE_H
