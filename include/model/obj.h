#ifndef OBJ_H
#define OBJ_H

#define OBJ 1
#define OBJECT 2
#define OBJ_NODE 3

#define CUBE 4

#include <iostream>
#include "vertex.h"

class Obj
{
protected:
    int Type;
public:
    Obj();
    int type();

    void add(Obj*);
    Obj* get(std::string);
    Obj* get(int);
    int size();

    void debug();
    std::string getName();
};

#endif // OBJ_H
