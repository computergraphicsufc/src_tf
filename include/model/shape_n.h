#ifndef SHAPE_N_H
#define SHAPE_N_H

#include "shape.h"

#include <vector>

class Shape_n : public Shape
{
private:
    std::vector<Shape*> group;
public:
    Shape_n(std::string);

    // inherited methods
    void add(Shape*);
    Shape* get(int);
    Shape* get(std::string);
    int type();
    std::string getName();
    int size();

    void print(int);
    Shape* duplicate();
    // inherited methods

    // entity method
    void transform(Matrix);
    // entity method

    // spherebound method
    float getSphereBound();
    Vertex getSphereCenter();
    // spherebound method
};

#endif // SHAPE_N_H

