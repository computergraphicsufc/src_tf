#ifndef MATERIAL_H
#define MATERIAL_H

#include"vertex.h"

class Material
{
private:
    Vertex* ambient, *difuse, *specular;
    float shininess, reflectivity, transparency, difraction;
public:
    Material(Vertex*, Vertex*,
             Vertex*, float, float, float);
    ~Material();

    void setDifraction(float);

    Vertex* getAmbient();
    Vertex* getDifuse();
    Vertex* getSpecular();
    float getShininess();
    float getReflectivity();
    float getTransparency();
    float getDifraction();

    friend std::ostream& operator << (std::ostream& os, Material m){
        os << " m\n";
        os << *m.ambient;
        os << *m.difuse;
        os << *m.specular;
        os << m.shininess << "\n";
        os << END << "m\n";
        return os;
    }
};

#endif // MATERIAL_H
