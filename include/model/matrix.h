#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>

class Matrix
{
private:
    float mat[4][4];
public:
    Matrix();
    Matrix(float,float,float,float,float,float,float,float,float,float,float,float,float,float,float,float);
    float get(int,int);
    void set(int,int,float);
/*
    friend std::ostream& operator << (std::ostream& os, Matrix M){
        os << "mx4\n";
        for(int index = 0 ; index < 4 ; index++)
            for(int index2 = 0 ; index2 < 4 ; index2++)
                os << M.mat[index][index2] << "\n";
        os << "?mx4\n";
        return os;
    }//operator
*/

    friend std::ostream& operator << (std::ostream& os, Matrix M){
        os << "Matrix = [\n";
        for(int index = 0 ; index < 4 ; index++){
            for(int index2 = 0 ; index2 < 4 ; index2++){
                os << M.mat[index][index2] << " ";
            }
            os << "\n";
        }
        os << "]";
        return os;
    }//operator


};

namespace matrix{
    Matrix identity();
}

#endif // MATRIX_H
