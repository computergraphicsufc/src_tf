#ifndef READER_H
#define READER_H

#include "include/model/scene.h"

class Reader{
public:
    Scene* read(std::string);
    Object* readObject(std::string);
    Vertex* readVertex(std::string);
    Material* readMaterial(std::string);
};

#endif
