#ifndef WRITER_H
#define WRITER_H

#include "include/model/scene.h"

class Writer{
public:
    void write(Scene*, std::string);
};

#endif
