#ifndef VERTEX_COMPONENT_H
#define VERTEX_COMPONENT_H

#include "include/model/object.h"
#include <cmath>

namespace vertex_fun{
    Vertex sum(Vertex, Vertex);
    Vertex inverse(Vertex);
    Vertex minus(Vertex,Vertex);
    Vertex mult(Vertex,Vertex);
    Vertex mult(float, Vertex);
    Vertex div(Vertex, float);

    float dot(Vertex,Vertex);
    float length(Vertex);
    Vertex norm(Vertex);
    Vertex cross(Vertex,Vertex);
} // vertex_fun

#endif
