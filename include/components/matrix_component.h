#ifndef MATRIX_COMPONENT_H
#define MATRIX_COMPONENT_H

#include "include/model/vertex.h"
#include "include/model/matrix.h"

namespace matrix_fun{
    Vertex mult(Matrix, Vertex);
    Matrix mult(Matrix, Matrix);
} // matrix_fun

#endif
