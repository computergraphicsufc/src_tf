#ifndef TRANSFORM_COMPONENT_H
#define TRANSFORM_COMPONENT_H

#include "include/model/matrix.h"
#include "include/model/vertex.h"

namespace transform{
    Matrix rotate(float, char);
    Matrix rotate(float, Vertex);
    Matrix translate(Vertex);
    Matrix scale(Vertex, Vertex);
    Matrix sheer(char, char, float);
    Matrix mirror(Vertex);
} // transform

#endif
