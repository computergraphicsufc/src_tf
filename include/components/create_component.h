#ifndef CREATE_COMPONENT_H
#define CREATE_COMPONENT_H

#include "include/model/object.h"

namespace create{
    Object* cube(float);
    Object* piramid(float, float);
} // create

#endif
