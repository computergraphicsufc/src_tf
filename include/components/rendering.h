#ifndef RENDERING_H
#define RENDERING_H

#include "include/observer/camera.h"
#include "include/illumination/light.h"
#include "include/model/scene.h"

#include <vector>
#include "mainwindow.h"

#include <GL/glut.h>

namespace render{
    // basic shading methods
    void shading(Scene*);
    // basic shading methods

    // raycasting methods
    MatrixColor* raycasting(std::vector<Object*>, Camera*, Light*);
    // raycasting methods

    // raytracing methods
    MatrixColor* raytracing(Scene*);
    Vertex trace(Vertex,Vertex,float,Scene*);
    Vertex difraction(Vertex, Vertex, float);
    // raytracing methods

    Vertex* getColor(Vertex, Vertex, Vertex, Object*, Light*, Vertex);
    bool shadow(Vertex, Vertex,std::vector<Object*>);

    // intersection methods
    bool sphereIntersection(Vertex,Vertex,Object*);
    float intersec(Vertex, Vertex, Face*);
    float sceneIntersect(Vertex, Vertex, Scene*, Face**, Object**);

    bool groupIntersection(Vertex,Vertex, Shape*);
    float treeIntersection(Vertex, Vertex, Scene*, Face**, Object**);
    bool searchIntersection(Vertex, Vertex, Shape*, Object**);

    bool areaIntersection(Vertex, Vertex, std::vector<Object*>);
    // intersection methods

    // draw methods
    void display();
    void send(Scene*);
    Scene* get();
    void updateCamera();
    void constructImage();
    // draw methods

    void debug_draw(Shape*);

    void setWidget(MainWindow*);
    MainWindow* getWidget();


    // other functions
    float x(float, int, int);
    float y(float, int, int);
    // other functions
}// render

namespace projection{
    Matrix perspective(float,float,float,float,float,float);
}

#endif // RENDERING_H
