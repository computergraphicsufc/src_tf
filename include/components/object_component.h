#ifndef OBJECT_COMPONENT_H
#define OBJECT_COMPONENT_H

#include "include/model/object.h"

namespace object{
    Vertex centerGeometric(Object*);
}// object

#endif
