#ifndef PREDEFINED_H
#define PREDEFINED_H

#include "include/model/scene.h"

namespace construct{
    void room(Scene**);
    void mirroom(Scene**);
}// construct

namespace LEC{
    void chair(Scene**);
    void trash(Scene**);
    void table(Scene**);
    void armario(Scene**);
    void PC(Scene**);
    void profTable(Scene**);
    void bigTable(Scene**);
    void projector(Scene**);
    void projectorBoard(Scene**);
    void board(Scene**);

    void door(Scene**);

    void leftWall(Scene**);
    void rightWallOverDoor(Scene**);
    void rightWall(Scene**);
    void backfrontWall(Scene**);
    void ceilFloor(Scene**);

    void lineTable(Scene**);
    void lineTablePC(Scene**);
    void profTablePC(Scene**);

    void buildLEC(Scene**);
}

#endif
