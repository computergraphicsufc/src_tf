#ifndef SCREENSHOT_H
#define SCREENSHOT_H

#include "include/model/object.h"
#include "include/model/matrixcolor.h"
#include <vector>

/** V 0.1 */
/** NOT COMPLETE */

class Screenshot
{
private:
    float distance;
    float screenW, screenH;
    int windowW, windowH;
    MatrixColor* matrix;

public:
    Screenshot(float, float, float, float, float);
    ~Screenshot();
    void normalizeColors();
    float getDistance();
    float getScreenWidth();
    float getScreenHeight();
    int getWindowWidth();
    int getWindowHeight();
    MatrixColor* getMatrixColor();

    friend std::ostream& operator << (std::ostream& os, Screenshot s){
        os << "sc\n";
            os << s.distance << "\n";
            os << s.screenW << "\n";
            os << s.screenH << "\n";
            os << s.windowW << "\n";
            os << s.windowH << "\n";
        os << "?sc\n";
        return os;
    }//
};

#endif // SCREENSHOT_H
