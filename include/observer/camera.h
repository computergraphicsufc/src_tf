#ifndef CAMERA_H
#define CAMERA_H

#include "include/components/vertex_component.h"
#include "include/model/matrix.h"
#include "screenshot.h"

#include "include/model/entity.h"

/** V 0.1 */
/** NOT COMPLETE */

class Camera:public Entity
{
private:
    Vertex *position, *viewUp, *lookAt;
    Vertex *ic, *jc, *kc;
    Matrix* wc, *cw, *projection;
    Screenshot* screen;
public:
    Camera(Vertex*,Vertex*,Vertex*);
    ~Camera();

    Vertex* getPosition();
    Vertex* getVup();
    Vertex* getLookAt();
    Vertex* getIc();
    Vertex* getJc();
    Vertex* getKc();
    Matrix* getWC();
    Matrix* getCW();
    Matrix* getProjection();
    Screenshot* getScreenshot();

    void setScreenshot(Screenshot*);

    void setPosition(Vertex);
    void setViewUp(Vertex);
    void setLookAt(Vertex);

    // entity method
    void transform(Matrix);
    // entity method

    // prototype
    void convert(Object**);
    void toWorld(Object**);
    void project(Object**);

    void update();


    friend std::ostream& operator << (std::ostream& os, Camera* c){

        os << "c\n";
            os << *c->position;
            os << *c->lookAt;
            os << *c->viewUp;
            os << *c->wc;
            os << *c->cw;
        os << END << "c\n";
        return os;
    }

/*
    friend std::ostream& operator << (std::ostream& os, Camera c){
        os << "Camera:\n";
        os << "Position: " << *c.position << "\n";
        os << "lookAt: " << *c.lookAt << "\n";
        os << "view up: " << *c.viewUp << "\n";
        os << "world-to-cam:\n";
        os << *c.wc;
        os << "\ncam-to-world:\n";
        os << *c.cw;
        os << "\n";
        return os;
    }
*/
};

#endif // CAMERA_H
