#ifndef LIGHT_H
#define LIGHT_H

#include "include/model/vertex.h"
#include "include/model/entity.h"

class Light: public Entity
{
public:
    Light(Vertex*,Vertex*,Vertex*,Vertex*);
    ~Light();

    // setters
    void setAmbient(Vertex*);
    void setDifuse(Vertex*);
    void setSpecular(Vertex*);
    void setPosition(Vertex*);

    // getters
    Vertex* getAmbient();
    Vertex* getDifuse();
    Vertex* getSpecular();
    Vertex* getPosition();

    // entity method
    void transform(Matrix);
    // entity method

    friend std::ostream& operator << (std::ostream& os, Light l){
        os << "l\n";
        os << *l.position;
        os << *l.ambient;
        os << *l.difuse;
        os << *l.specular;
        os << END << "l\n";
        return os;
    }//tostring
/*
    friend std::ostream& operator << (std::ostream& os, Light l){
        os << "Position: " << *l.position << std::endl;
        os << "Ambient: " << *l.ambient << std::endl;
        os << "Difuse: " << *l.difuse << std::endl;
        os << "Specular: " << *l.specular << std::endl;

        return os;
    }//tostring
*/
private:
    Vertex *ambient, *difuse, *specular, *position;
};

#endif // LIGHT_H
