#ifndef PIRAMID_H
#define PIRAMID_H

#include "include/model/object.h"

class Piramid : public Object{
    public:
        Piramid(std::string, float, float);
        float getSphereBound();
        Vertex getSphereCenter();
};

#endif
