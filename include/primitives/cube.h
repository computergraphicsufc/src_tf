#ifndef CUBE_H
#define CUBE_H

#include "include/model/object.h"

class Cube : public Object{
    public:
        Cube(std::string, float);
        float getSphereBound();
        Vertex getSphereCenter();
};

#endif
