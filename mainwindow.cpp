#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "render.h"

#include "include/components/rendering.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::up(){
    Scene* s = render::get();
    s->camworld();
    s->girate(G_UP);
    s->worldcam();
}

void MainWindow::down(){
    Scene* s = render::get();
    s->camworld();
    s->girate(G_DOWN);
    s->worldcam();
}

void MainWindow::left(){
    Scene* s = render::get();
    s->camworld();
    s->girate(G_LEFT);
    s->worldcam();
}

void MainWindow::right(){
    Scene* s = render::get();
    s->camworld();
    s->girate(G_RIGHT);
    s->worldcam();
}

void MainWindow::rw(){
    Scene* s = render::get();
    s->camworld();
    s->walk(RIGHT);
    s->worldcam();
}

void MainWindow::lw(){
    Scene* s = render::get();
    s->camworld();
    s->walk(LEFT);
    s->worldcam();
}

void MainWindow::uw(){
    Scene* s = render::get();
    s->camworld();
    s->walk(FRONT);
    s->worldcam();
}

void MainWindow::dw(){
    Scene* s = render::get();
    s->camworld();
    s->walk(BACK);
    s->worldcam();
}

void MainWindow::render(){
    MatrixColor* m = render::raytracing(render::get());

    Render* r = new Render();
    r->raytrace(m);
    r->show();
}

void MainWindow::camera()
{
    float eyex = this->ui->eyex->text().toFloat();
    float eyey = this->ui->eyey->text().toFloat();
    float eyez = this->ui->eyez->text().toFloat();

    float lookx = this->ui->lookx->text().toFloat();
    float looky = this->ui->looky->text().toFloat();
    float lookz = this->ui->lookz->text().toFloat();

    float vupx = this->ui->vupx->text().toFloat();
    float vupy = this->ui->vupy->text().toFloat();
    float vupz = this->ui->vupz->text().toFloat();

    render::get()->camworld();
    render::get()->setCamera(new Camera(new Vertex(eyex, eyey, eyez),
                                        new Vertex(lookx, looky, lookz),
                                        new Vertex(vupx, vupy, vupz)));
    render::get()->worldcam();
}//render

void MainWindow::setProgress(float a){
    this->ui->progressBar->setValue(a);
}
